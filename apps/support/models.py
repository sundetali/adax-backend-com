from django.db import models
from safedelete.models import SafeDeleteModel


class Message(SafeDeleteModel):
    email = models.EmailField(max_length=512)
    text = models.CharField(max_length=2048)

    def __str__(self):
        return self.email

    class Meta:
        db_table = 'api_support_message'
        verbose_name = 'message'
        verbose_name_plural = 'messages'
