from rest_framework import serializers

from apps.support.models import Message


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'


class MessageCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ('email', 'text')


class MessageRetrieveSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ('pk', 'email', 'text')
