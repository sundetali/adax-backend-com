from rest_framework import status
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from apps.support.models import Message
from apps.support.serializers import MessageCreateSerializer, MessageRetrieveSerializer


class CreateSupportMessageView(CreateAPIView):
    queryset = Message.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = MessageCreateSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        message = self.perform_create(serializer)
        read_serializer = MessageRetrieveSerializer(message)

        headers = self.get_success_headers(read_serializer.data)
        return Response(read_serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        return serializer.save()
