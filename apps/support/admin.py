from django.contrib import admin

from apps.support.models import Message

admin.site.register(
    [
        Message
    ]
)
