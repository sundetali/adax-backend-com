from django.urls import path

from apps.support.views import CreateSupportMessageView

app_name = "support"

urlpatterns = [
    path('message/', CreateSupportMessageView.as_view(), name='support_message')
]
