from typing import Any, Dict

from apps.notification.services import create_notification_base
from apps.users.models import User


@create_notification_base
def change_name_notification(user: User) -> Dict[str, Any]:
    return {'name': 'ФИО изменено'}


@create_notification_base
def change_pass_notification(user: User) -> Dict[str, Any]:
    return {'name': 'Пароль успешно изменен'}
