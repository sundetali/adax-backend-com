from django.utils.crypto import get_random_string
from rest_framework import serializers

from apps.users.models import User
from apps.users.tasks import change_pass_notification


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


class UserRegistrationSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=512, write_only=True)

    class Meta:
        model = User
        fields = ('email', 'phone', 'password')

    def validate(self, attrs):
        attrs['confirm_key'] = get_random_string(length=32)
        return attrs

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)


class UserRetrieveSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'email', 'phone', 'first_name', 'last_name')


class UserNameEditSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['first_name', 'last_name']


class UserEmailEditSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['email']

    def validate(self, attrs):
        attrs['confirm_key'] = get_random_string(length=32)
        attrs['email_confirmed'] = False
        return attrs


class UserPasswordEditSerializer(serializers.Serializer):
    current_password = serializers.CharField(max_length=512, write_only=True)
    password = serializers.CharField(max_length=512, write_only=True)
    confirm_password = serializers.CharField(max_length=512, write_only=True)

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass

    def validate(self, data):
        current_password = data.get('current_password', None)
        password = data.get('password', None)
        confirm_password = data.get('confirm_password', None)

        user = self.context['request'].user

        if not user.check_password(current_password):
            raise serializers.ValidationError('Invalid password')

        if password != confirm_password:
            raise serializers.ValidationError(
                'Passwords doesnt match.'
            )

        user.set_password(password)
        user.save()
        change_pass_notification(user)
        return user
