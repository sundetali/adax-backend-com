from django.contrib.sites.shortcuts import get_current_site
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from adax.utils import Mailer
from apps.users.serializers import UserRetrieveSerializer, UserNameEditSerializer, UserEmailEditSerializer, \
    UserPasswordEditSerializer
from apps.users.tasks import change_pass_notification, change_name_notification


class RetrieveUserView(GenericAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserRetrieveSerializer

    def get(self, request, *args, **kwargs):
        user = request.user
        read_serializer = self.get_serializer(user)

        return Response(
            read_serializer.data,
            status=status.HTTP_200_OK,
        )


class EditUserNameView(GenericAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserNameEditSerializer

    def put(self, request, *args, **kwargs):
        user = request.user
        new_user = self.get_serializer(user, data=request.data)
        assert new_user.is_valid(raise_exception=True)
        user = new_user.save()
        change_name_notification(user)
        return Response(
            data=UserRetrieveSerializer(user).data,
            status=status.HTTP_200_OK,
        )


class EditUserEmailView(GenericAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserEmailEditSerializer

    def put(self, request, *args, **kwargs):
        user = request.user
        new_user = self.get_serializer(user, data=request.data)
        assert new_user.is_valid(raise_exception=True)
        user = new_user.save()

        mailer = Mailer(mail_type='confirm_email', user=user)
        mailer.send_email(domain=get_current_site(request))

        return Response(
            data=UserRetrieveSerializer(user).data,
            status=status.HTTP_200_OK,
        )


class EditUserPasswordView(GenericAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserPasswordEditSerializer

    def put(self, request, *args, **kwargs):
        user = request.user
        new_user = self.get_serializer(user, data=request.data)
        assert new_user.is_valid(raise_exception=True)

        return Response(
            data=UserRetrieveSerializer(user).data,
            status=status.HTTP_200_OK,
        )
