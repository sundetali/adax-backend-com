from django.urls import path

from apps.users.views import RetrieveUserView, EditUserNameView, EditUserEmailView, EditUserPasswordView

app_name = "users"

urlpatterns = [
    path('me/', RetrieveUserView.as_view(), name='user_retrieve'),
    path('change-name/', EditUserNameView.as_view(), name='user_edit_name'),
    path('change-email/', EditUserEmailView.as_view(), name='user_edit_email'),
    path('change-password/', EditUserPasswordView.as_view(), name='user_edit_password')
]
