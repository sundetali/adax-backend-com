from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer

from apps.notification.models import Notification
from apps.users.models import User


def create_notification_base(func):
    def wrap(user: User, *args, **kwargs):
        text = func(user, *args, **kwargs)
        Notification.objects.create(user=user, text=text)
        async_to_sync(get_channel_layer().group_send)(
            str(user.id),
            {
                'type': 'send_notification',
                'text': text
            }
        )

    return wrap
