from django.urls import path

from apps.notification.views import room, NotificationView

app_name = 'notification'
urlpatterns = [
    path('<int:user_id>/', room),
    path('', NotificationView.as_view())
]