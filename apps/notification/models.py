from django.db import models
from django.contrib.postgres.fields import JSONField

from apps.users.models import User


class Notification(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='notifications')
    text = models.JSONField()
    created = models.DateTimeField(auto_now_add=True)
