from django.shortcuts import render

from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated

from adax.permissions import IsConfirmedUser
from apps.notification.models import Notification
from apps.notification.serialisers import NotificationRetrieveSerializer


def room(request, user_id):
    return render(request, 'notification/room.html', {'user_id': user_id})


class NotificationView(ListAPIView):
    queryset = Notification.objects.all()
    serializer_class = NotificationRetrieveSerializer
    permission_classes = [IsAuthenticated, IsConfirmedUser]

    def get_queryset(self):
        return self.queryset.filter(user=self.request.user)
