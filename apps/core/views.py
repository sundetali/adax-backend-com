from rest_framework.mixins import ListModelMixin
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.viewsets import GenericViewSet, ModelViewSet

from adax.permissions import IsAdmin
from apps.core.models import City, Category, AdvertisementType
from apps.core.serializers import CityListSerializer, AdvertisementTypeListSerializer, CategoryListSerializer, \
    CitySerializer, CategorySerializer


class CityViewSet(GenericViewSet, ListModelMixin):
    queryset = City.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = CityListSerializer


class CityAdminViewSet(ModelViewSet):
    queryset = City.objects.all()
    permission_classes = (IsAuthenticated, IsAdmin,)
    serializer_class = CityListSerializer


class AdvertisementTypeViewSet(GenericViewSet, ListModelMixin):
    queryset = AdvertisementType.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = AdvertisementTypeListSerializer


class CategoryViewSet(GenericViewSet, ListModelMixin):
    queryset = Category.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = CategoryListSerializer


class CategoryAdminViewSet(ModelViewSet):
    queryset = Category.objects.all()
    permission_classes = (IsAuthenticated, IsAdmin,)
    serializer_class = CategoryListSerializer
