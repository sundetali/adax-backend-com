from rest_framework import serializers

from apps.core.models import City, Category, AdvertisementType


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = '__all__'


class CityRetrieveSerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ('pk', 'title')


class CityListSerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ('pk', 'title')


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class CategoryRetrieveSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('pk', 'title')


class CategoryListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('pk', 'title')


class AdvertisementTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdvertisementType
        fields = '__all__'


class AdvertisementTypeRetrieveSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdvertisementType
        fields = ('pk', 'title')


class AdvertisementTypeListSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdvertisementType
        fields = ('pk', 'title')
