from django.contrib import admin

from apps.core.models import City, Category, AdvertisementType

admin.site.register(
    [
        City,
        Category,
        AdvertisementType
    ]
)
