from django.db import models
from django.utils import timezone
from safedelete.models import SafeDeleteModel


class City(SafeDeleteModel):
    title = models.CharField(max_length=512)

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'api_core_city'
        verbose_name = 'city'
        verbose_name_plural = 'cities'


class Category(SafeDeleteModel):
    title = models.CharField(max_length=512)

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'api_core_category'
        verbose_name = 'category'
        verbose_name_plural = 'categories'


class AdvertisementType(SafeDeleteModel):
    title = models.CharField(max_length=256)

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'api_core_advertisement_type'
        verbose_name = 'advertisement type'
        verbose_name_plural = 'advertisement types'


class TimeStampedModel(models.Model):
    """
    An abstract base class model that provides self-
    updating ``created_at``.
    """

    created_at = models.DateTimeField(default=timezone.now)

    class Meta:
        abstract = True
