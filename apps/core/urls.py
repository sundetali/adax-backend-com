from django.urls import path, include
from rest_framework import routers

from apps.core.views import CityViewSet, AdvertisementTypeViewSet, CategoryViewSet, CityAdminViewSet, \
    CategoryAdminViewSet

app_name = "core"

admin_router = routers.DefaultRouter()
admin_router.register(r'cities', CityAdminViewSet, basename='business_admin')
admin_router.register(r'categories', CategoryAdminViewSet, basename='business_admin')

router = routers.DefaultRouter()
router.register(r'cities', CityViewSet)
router.register(r'advertisement-types', AdvertisementTypeViewSet)
router.register(r'categories', CategoryViewSet)

urlpatterns = [
    path('admin/', include(admin_router.urls)),
    path('', include(router.urls)),
]
