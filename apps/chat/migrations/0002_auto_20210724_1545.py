# Generated by Django 3.1.6 on 2021-07-24 15:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('advertisements', '0007_auto_20210429_1455'),
        ('chat', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Chat',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('index', models.IntegerField()),
            ],
            options={
                'managed': False,
            },
        ),
        migrations.AddField(
            model_name='lastseenmessage',
            name='advertisement',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='last_messages', to='advertisements.advertisement'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='message',
            name='advertisement',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='messages', to='advertisements.advertisement'),
            preserve_default=False,
        ),
    ]
