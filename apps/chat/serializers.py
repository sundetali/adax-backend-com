from rest_framework import serializers

from apps.advertisements.serializers import AdvertisementPkTitleSerializer
from apps.chat.models import Message, Chat
from apps.users.serializers import UserRetrieveSerializer


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ('id', 'sender', 'receiver', 'advertisement', 'message_text', 'created_at')


class ChatSerializer(serializers.ModelSerializer):
    user = UserRetrieveSerializer()
    advertisement = AdvertisementPkTitleSerializer()

    class Meta:
        model = Chat
        fields = ('user', 'advertisement')
