from django.db import models
from safedelete import SOFT_DELETE_CASCADE
from safedelete.models import SafeDeleteModel

from apps.advertisements.models import Advertisement
from apps.core.models import TimeStampedModel
from apps.users.models import User


class Message(SafeDeleteModel, TimeStampedModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    sender = models.ForeignKey(User, related_name='sent_messages', on_delete=models.CASCADE)
    receiver = models.ForeignKey(User, related_name='received_messages', on_delete=models.CASCADE)
    advertisement = models.ForeignKey(Advertisement, related_name='messages', on_delete=models.CASCADE)
    message_text = models.CharField(max_length=1000)

    class Meta:
        db_table = 'api_chat_message'
        verbose_name = 'message'
        verbose_name_plural = 'messages'


class Chat(models.Model):
    index = models.IntegerField()
    user = models.ForeignKey(User, related_name='chats', on_delete=models.CASCADE)
    advertisement = models.ForeignKey(Advertisement, related_name='chats', on_delete=models.CASCADE)

    class Meta:
        managed = False

    def __hash__(self):
        return hash(('user_id', self.user_id,
                     'advertisement_id', self.advertisement_id))

    def __eq__(self, other):
        return self.user_id == other.user_id \
               and self.advertisement_id == other.advertisement_id


class LastSeenMessage(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE

    user = models.ForeignKey(User, related_name='user_last_messages', on_delete=models.CASCADE)
    chat = models.ForeignKey(User, related_name='chat_last_messages', on_delete=models.CASCADE)
    advertisement = models.ForeignKey(Advertisement, related_name='last_messages', on_delete=models.CASCADE)
    message = models.ForeignKey(Message, related_name='last_seens', on_delete=models.DO_NOTHING)

    class Meta:
        db_table = 'api_chat_last_seen_message'
        verbose_name = 'last seen message'
        verbose_name_plural = 'last seen messages'
