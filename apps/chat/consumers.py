from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer
from rest_framework.utils import json

from apps.advertisements.models import Advertisement
from apps.chat.models import LastSeenMessage, Message
from apps.chat.serializers import MessageSerializer
from apps.users.models import User


class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = f"{self.scope['url_route']['kwargs']['user_id']}_{self.scope['url_route']['kwargs']['advertisement_id']}"

        if self.scope['user'] and self.scope['user'].pk:
            user_id = self.scope['user'].pk
            chat_id = self.room_name.split('_')[0]
            advertisement_id = self.room_name.split('_')[1]
            self.room_group_name = f"{user_id}_{self.room_name}"

            # Join chat group
            await self.channel_layer.group_add(
                self.room_group_name,
                self.channel_name
            )

            await self.accept()

            await self.send(text_data=json.dumps({
                'messages': await self.get_history(user_id, chat_id, advertisement_id),
                'last_message_seen': await self.get_last_seen_message(user_id, chat_id, advertisement_id)
            }))
        else:
            await self.close()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    async def receive(self, text_data=None, **kwargs):
        message = text_data.strip()
        user_id = int(self.scope['user'].pk)
        chat_id = int(self.room_name.split('_')[0])
        advertisement_id = int(self.room_name.split('_')[1])

        if message:
            message_data = json.loads(message)
            message_type = message_data['type']

            if message_type == 'scroll':
                message_id = message_data['message']

                await self.send(text_data=json.dumps({
                    'messages': await self.get_scroll_messages(user_id, chat_id, advertisement_id, message_id),
                    'last_message_seen': await self.get_last_seen_message(user_id, chat_id, advertisement_id)
                }))
            elif message_type == 'message':
                text = message_data['message']

                message = await self.create_message(user_id, chat_id, advertisement_id, text)

                # Send message to room group
                await self.channel_layer.group_send(
                    f"{self.room_group_name.split('_')[1]}_{self.room_group_name.split('_')[0]}_{self.room_group_name.split('_')[2]}",
                    {
                        'type': 'chat_message',
                        'message': message
                    }
                )

                await self.channel_layer.group_send(
                    self.room_group_name.split('_')[1],
                    {
                        'type': 'chat_message',
                        'message': message
                    }
                )

                await self.send(text_data=json.dumps({
                    'message': message
                }))

            if message_type == 'seen_message':
                message_id = message_data['message']

                await self.send(text_data=json.dumps({
                    'last_message_seen': await self.set_last_seen_message(user_id, chat_id, advertisement_id, message_id),
                }))
        else:
            pass

    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message
        }))

    @database_sync_to_async
    def create_message(self, user_id, chat_id, advertisement_id, text):
        message_data = {
            'sender': User.objects.filter(pk=user_id).first(),
            'receiver': User.objects.filter(pk=chat_id).first(),
            'advertisement': Advertisement.objects.filter(pk=advertisement_id).first(),
            'message_text': text
        }

        message = Message.objects.create(**message_data)
        return MessageSerializer(message).data

    @database_sync_to_async
    def set_last_seen_message(self, user_id, chat_id, advertisement_id, message_id):
        last_message = LastSeenMessage.objects.filter(user__id=user_id, chat__id=chat_id,
                                                      advertisement__id=advertisement_id).first()

        if last_message:
            last_message.message = Message.objects.filter(pk=message_id).first()
            last_message.save()

        last_message_data = {
            'user': User.objects.filter(pk=user_id).first(),
            'chat': User.objects.filter(pk=chat_id).first(),
            'advertisement': Advertisement.objects.filter(pk=advertisement_id).first(),
            'message': Message.objects.filter(pk=message_id).first()
        }

        last_message = LastSeenMessage.objects.create(**last_message_data)
        return last_message.message.id

    @database_sync_to_async
    def get_last_seen_message(self, user_id, chat_id, advertisement_id):
        last_message = LastSeenMessage.objects.filter(user__id=user_id, chat__id=chat_id,
                                                      advertisement__id=advertisement_id).first()

        if last_message:
            return last_message.message.id

        return None

    @database_sync_to_async
    def get_scroll_messages(self, user_id, chat_id, advertisement_id, message_id):
        scroll_messages = Message.objects.filter(sender__id__in=[user_id, chat_id],
                                                 receiver__id__in=[user_id, chat_id],
                                                 advertisement__id=advertisement_id,
                                                 id__lt=message_id).order_by('-id')
        messages = scroll_messages[:20]

        return MessageSerializer(messages, many=True).data

    @database_sync_to_async
    def get_history(self, user_id, chat_id, advertisement_id):
        last_message = LastSeenMessage.objects.filter(user__id=user_id, chat__id=chat_id, advertisement__id=advertisement_id).first()

        chat_messages = Message.objects.filter(sender__id__in=[user_id, chat_id],
                                               receiver__id__in=[user_id, chat_id],
                                               advertisement__id=advertisement_id).order_by('-id')
        if last_message:
            old_messages = chat_messages.filler(id__lte=last_message.id)[:20]
            new_messages = chat_messages.filter(id__gte=last_message.id)

            messages = new_messages.union(old_messages)
        else:
            messages = chat_messages[:20]

        return MessageSerializer(messages, many=True).data


class ChatNotifyConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        if self.scope['user'] and self.scope['user'].pk:
            user_id = self.scope['user'].pk
            self.room_group_name = f"{user_id}"

            # Join chat group
            await self.channel_layer.group_add(
                self.room_group_name,
                self.channel_name
            )

            await self.accept()
        else:
            await self.close()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message
        }))
