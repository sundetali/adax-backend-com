from django.contrib import admin

from apps.chat.models import Message, LastSeenMessage

admin.site.register(
    [
        Message,
        LastSeenMessage
    ]
)
