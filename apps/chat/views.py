from django.db.models import Q
from rest_framework.mixins import ListModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import GenericViewSet

from adax.permissions import IsConfirmedUser
from apps.advertisements.models import Advertisement
from apps.chat.models import Message, Chat
from apps.chat.serializers import ChatSerializer
from apps.users.models import User


class ChatViewSet(GenericViewSet, ListModelMixin):
    permission_classes = (IsAuthenticated, IsConfirmedUser,)
    serializer_class = ChatSerializer
    queryset = Message.objects.all()

    def get_queryset(self):
        received_chats = self.queryset.filter(Q(sender__id=self.request.user.id))
        sent_chats = self.queryset.filter(Q(receiver__id=self.request.user.id))

        receivers = list(received_chats.values_list('id', 'receiver__id', 'advertisement__id').distinct('receiver__id', 'advertisement__id'))
        senders = list(sent_chats.values_list('id', 'sender__id', 'advertisement__id').distinct('sender__id', 'advertisement__id'))

        all_chats = [Chat(index=i[0], user=User.objects.get(pk=i[1]), advertisement=Advertisement.objects.get(pk=i[2])) for i in receivers + senders]
        all_chats = sorted(all_chats, key=lambda chat: -chat.index)
        all_chats = set(all_chats)

        return all_chats
