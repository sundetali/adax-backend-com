from django.urls import path, include
from rest_framework import routers

from apps.chat.views import ChatViewSet

app_name = "chat"

router = routers.DefaultRouter()
router.register(r'', ChatViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
