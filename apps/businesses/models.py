import os

from django.core.validators import RegexValidator
from django.db import models
from django.dispatch import receiver
from safedelete.models import SafeDeleteModel

from apps.advertisements.models import Advertisement
from apps.core.models import Category, City


class Business(SafeDeleteModel):
    advertisement = models.OneToOneField(Advertisement, related_name='businesses', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id)

    class Meta:
        db_table = 'api_businesses_business'
        verbose_name = 'business'
        verbose_name_plural = 'businesses'


class BusinessBasic(SafeDeleteModel):
    name = models.CharField(max_length=1024)
    price = models.IntegerField()
    category = models.ForeignKey(Category, on_delete=models.DO_NOTHING)
    city = models.ForeignKey(City, on_delete=models.DO_NOTHING)
    years_on_market = models.IntegerField()
    business = models.OneToOneField(Business, related_name='basic_info', on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'api_businesses_business_basic'
        verbose_name = 'business basic'
        verbose_name_plural = 'business basic'


class BusinessBasicMedia(SafeDeleteModel):
    image = models.ImageField(upload_to='media/')
    business_basic = models.ForeignKey(BusinessBasic, on_delete=models.CASCADE, related_name='media')

    def __str__(self):
        return str(self.business_basic)

    class Meta:
        db_table = 'api_businesses_business_basic_media'
        verbose_name = 'business basic media'
        verbose_name_plural = 'business basic media'


@receiver(models.signals.post_delete, sender=BusinessBasicMedia)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    if instance.image:
        if os.path.isfile(instance.image.path):
            os.remove(instance.image.path)


class BusinessFinancial(SafeDeleteModel):
    earnings = models.IntegerField()
    spending = models.IntegerField()
    share = models.IntegerField()
    borrowed_funds = models.CharField(max_length=2048, null=True, blank=True)
    goods_remainder = models.CharField(max_length=2048, null=True, blank=True)
    estimated_payback_year = models.IntegerField()
    estimated_payback_month = models.IntegerField()
    business = models.OneToOneField(Business, related_name='financial_info', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.business) if hasattr(self, 'business') else str(self.id)

    class Meta:
        db_table = 'api_businesses_business_financial'
        verbose_name = 'business financial'
        verbose_name_plural = 'business financials'


class BusinessAdditional(SafeDeleteModel):
    BUILDINGS = (
        (1, 'В аренде'),
        (2, 'В собственности'),
        (3, 'Не требуется'),
    )

    buildings = models.IntegerField(choices=BUILDINGS)
    staff_count = models.IntegerField(null=True, blank=True)
    production_means = models.CharField(max_length=2048, null=True, blank=True)
    rent_expiration_year = models.IntegerField(null=True, blank=True)
    rent_expiration_month = models.IntegerField(null=True, blank=True)
    rent_extend_right = models.BooleanField(null=True, blank=True)
    property_description = models.CharField(max_length=2048, null=True, blank=True)
    business = models.OneToOneField(Business, related_name='additional_info', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.business) if hasattr(self, 'business') else str(self.id)

    class Meta:
        db_table = 'api_businesses_business_additional'
        verbose_name = 'business additional'
        verbose_name_plural = 'business additional'


class BusinessAdditionalDocument(SafeDeleteModel):
    file = models.FileField(upload_to='documents/')
    business_additional = models.ForeignKey(BusinessAdditional, on_delete=models.CASCADE, related_name='documents')

    def __str__(self):
        return str(self.business_additional)

    class Meta:
        db_table = 'api_businesses_business_additional_document'
        verbose_name = 'business additional document'
        verbose_name_plural = 'business additional documents'


@receiver(models.signals.post_delete, sender=BusinessAdditionalDocument)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    if instance.file:
        if os.path.isfile(instance.file.path):
            os.remove(instance.file.path)


class BusinessContact(SafeDeleteModel):
    contact = models.CharField(max_length=512)
    email = models.EmailField(max_length=512)
    phone_regex = RegexValidator(regex=r'^(7)([0-9]{10})$',
                                 message="Phone number must be entered in the format: '77077070077'. Up to 11 digits "
                                         "allowed.")
    phone = models.CharField(validators=[phone_regex], max_length=11)
    business_description = models.CharField(max_length=2048)
    business = models.OneToOneField(Business, related_name='contact_info', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.business) if hasattr(self, 'business') else str(self.id)

    class Meta:
        db_table = 'api_businesses_business_contact'
        verbose_name = 'business contact'
        verbose_name_plural = 'business contacts'
