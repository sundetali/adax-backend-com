from django.contrib import admin

from apps.businesses.models import BusinessBasic, BusinessBasicMedia, BusinessFinancial, BusinessAdditional, \
    BusinessAdditionalDocument, BusinessContact, Business

admin.site.register(
    [
        BusinessBasic,
        BusinessBasicMedia,
        BusinessFinancial,
        BusinessAdditional,
        BusinessAdditionalDocument,
        BusinessContact,
        Business
    ]
)
