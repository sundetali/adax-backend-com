from django.urls import path, include
from rest_framework import routers

from apps.businesses.views import BusinessViewSet, BusinessAdminViewSet, BusinessBasicMediaUploadViewSet, \
    BusinessBasicMediaDeleteViewSet, BusinessAdditionalDocumentsUploadViewSet, BusinessAdditionalDocumentsDeleteViewSet

app_name = "businesses"

admin_router = routers.DefaultRouter()
admin_router.register(r'', BusinessAdminViewSet, basename='business_admin')


router = routers.DefaultRouter()
router.register(r'', BusinessViewSet, basename='business')

urlpatterns = [
    path('basic-info/<int:pk>/upload-media/', BusinessBasicMediaUploadViewSet.as_view(),
         name='business_basic_media_upload'),
    path('basic-info/<int:pk>/delete-media/', BusinessBasicMediaDeleteViewSet.as_view(),
         name='business_basic_media_delete'),
    path('additional-info/<int:pk>/upload-documents/', BusinessAdditionalDocumentsUploadViewSet.as_view(),
         name='business_additional_documents_upload'),
    path('additional-info/<int:pk>/delete-documents/', BusinessAdditionalDocumentsDeleteViewSet.as_view(),
         name='business_additional_documents_delete'),
    path('admin/', include(admin_router.urls)),
    path('', include(router.urls)),
]
