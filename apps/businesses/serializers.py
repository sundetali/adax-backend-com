from django.db import transaction
from rest_framework import serializers

from apps.advertisements.serializers import AdvertisementRetrieveSerializer, AdvertisementSerializer
from apps.businesses.models import BusinessBasic, BusinessBasicMedia, BusinessFinancial, BusinessAdditional, \
    BusinessAdditionalDocument, BusinessContact, Business
from apps.core.serializers import CategoryRetrieveSerializer, CityRetrieveSerializer


class BusinessBasicMediaSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessBasicMedia
        fields = ('image',)


class BusinessBasicMediaRetrieveSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessBasicMedia
        fields = ('id', 'image',)


class BusinessBasicMediaUploadSerializer(serializers.ModelSerializer):
    media = serializers.ListField(child=serializers.ImageField())

    class Meta:
        model = BusinessBasic
        fields = ('media',)

    def update(self, instance, validated_data):
        media_data_list = validated_data.get('media', [])

        for media in media_data_list:
            new_media = {
                'image': media
            }
            document = BusinessBasicMediaSerializer(data=new_media)
            assert document.is_valid(raise_exception=True)
            document.save(business_basic=instance)

        return instance


class BusinessBasicMediaDeleteSerializer(serializers.ModelSerializer):
    media_to_delete = serializers.ListField(child=serializers.IntegerField())

    class Meta:
        model = BusinessBasic
        fields = ('media_to_delete',)

    def update(self, instance, validated_data):
        ids_to_delete = validated_data.get('media_to_delete', [])

        for id_to_delete in ids_to_delete:
            item = instance.media.filter(id=id_to_delete)

            if item.exists():
                item.delete()

        return instance


class BusinessBasicRetrieveSerializer(serializers.ModelSerializer):
    category = CategoryRetrieveSerializer()
    city = CityRetrieveSerializer()
    media = BusinessBasicMediaRetrieveSerializer(many=True)

    class Meta:
        model = BusinessBasic
        fields = ('pk', 'name', 'price', 'category', 'city', 'years_on_market', 'media')


class BusinessBasicSerializer(serializers.ModelSerializer):

    class Meta:
        model = BusinessBasic
        fields = ('name', 'price', 'category', 'city', 'years_on_market')

    def create(self, validated_data):
        # Business Basic
        business_basic = BusinessBasic.objects.create(**validated_data)

        return business_basic


class BusinessBasicUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = BusinessBasic
        fields = ('name', 'price', 'category', 'city', 'years_on_market')

    def update(self, instance, validated_data):
        # Business Basic
        instance.name = validated_data.get('name', instance.name)
        instance.price = validated_data.get('price', instance.price)
        instance.category = validated_data.get('category', instance.category)
        instance.city = validated_data.get('city', instance.city)
        instance.years_on_market = validated_data.get('years_on_market', instance.years_on_market)
        instance.save()

        return instance


class BusinessFinancialRetrieveSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessFinancial
        fields = ('pk', 'earnings', 'spending', 'share', 'borrowed_funds', 'goods_remainder', 'estimated_payback_year',
                  'estimated_payback_month')


class BusinessFinancialSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessFinancial
        fields = ('earnings', 'spending', 'share', 'borrowed_funds', 'goods_remainder', 'estimated_payback_year',
                  'estimated_payback_month')


class BusinessFinancialUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessFinancial
        fields = ('earnings', 'spending', 'share', 'borrowed_funds', 'goods_remainder', 'estimated_payback_year',
                  'estimated_payback_month')


class BusinessAdditionalDocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessAdditionalDocument
        fields = ('file',)


class BusinessAdditionalDocumentRetrieveSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessAdditionalDocument
        fields = ('id', 'file',)


class BusinessAdditionalDocumentsUploadSerializer(serializers.ModelSerializer):
    documents = serializers.ListField(child=serializers.FileField())

    class Meta:
        model = BusinessAdditional
        fields = ('documents',)

    def update(self, instance, validated_data):
        documents_data_list = validated_data.get('documents', [])

        for document in documents_data_list:
            new_document = {
                'file': document
            }
            document = BusinessAdditionalDocumentSerializer(data=new_document)
            assert document.is_valid(raise_exception=True)
            document.save(business_additional=instance)

        return instance


class BusinessAdditionalDocumentsDeleteSerializer(serializers.ModelSerializer):
    documents_to_delete = serializers.ListField(child=serializers.IntegerField())

    class Meta:
        model = BusinessAdditional
        fields = ('documents_to_delete',)

    def update(self, instance, validated_data):
        ids_to_delete = validated_data.get('documents_to_delete', [])

        for id_to_delete in ids_to_delete:
            item = instance.documents.filter(id=id_to_delete)

            if item.exists():
                item.delete()

        return instance


class BusinessAdditionalRetrieveSerializer(serializers.ModelSerializer):
    buildings = serializers.CharField(source='get_buildings_display')
    documents = BusinessAdditionalDocumentRetrieveSerializer(many=True)

    class Meta:
        model = BusinessAdditional
        fields = ('pk', 'staff_count', 'production_means', 'buildings', 'rent_expiration_year',
                  'rent_expiration_month', 'rent_extend_right', 'property_description', 'documents')


class BusinessAdditionalSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessAdditional
        fields = ('staff_count', 'production_means', 'buildings', 'rent_expiration_year',
                  'rent_expiration_month', 'rent_extend_right', 'property_description')

    def create(self, validated_data):
        # Business Additional
        business_additional = BusinessAdditional.objects.create(**validated_data)

        return business_additional


class BusinessAdditionalUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessAdditional
        fields = ('staff_count', 'production_means', 'buildings', 'rent_expiration_year',
                  'rent_expiration_month', 'rent_extend_right', 'property_description')

    def update(self, instance, validated_data):
        # Business Additional
        instance.staff_count = validated_data.get('staff_count', instance.staff_count)
        instance.production_means = validated_data.get('production_means', instance.production_means)
        instance.buildings = validated_data.get('buildings', instance.buildings)
        instance.rent_expiration_year = validated_data.get('rent_expiration_year', instance.rent_expiration_year)
        instance.rent_expiration_month = validated_data.get('rent_expiration_month', instance.rent_expiration_month)
        instance.rent_extend_right = validated_data.get('rent_extend_right', instance.rent_extend_right)
        instance.property_description = validated_data.get('property_description', instance.property_description)
        instance.save()

        return instance


class BusinessContactRetrieveSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessContact
        fields = ('pk', 'contact', 'email', 'phone', 'business_description', )


class BusinessContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessContact
        fields = ('contact', 'email', 'phone', 'business_description', )


class BusinessContactUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessContact
        fields = ('contact', 'email', 'phone', 'business_description', )


class BusinessRetrieveSerializer(serializers.ModelSerializer):
    advertisement = AdvertisementRetrieveSerializer()
    basic_info = BusinessBasicRetrieveSerializer()
    financial_info = BusinessFinancialRetrieveSerializer()
    additional_info = BusinessAdditionalRetrieveSerializer()
    contact_info = BusinessContactRetrieveSerializer()

    class Meta:
        model = Business
        fields = ('pk', 'advertisement', 'basic_info', 'financial_info', 'additional_info', 'contact_info')


class BusinessSearchRetrieveSerializer(serializers.ModelSerializer):
    advertisement = AdvertisementRetrieveSerializer()
    basic_info = BusinessBasicRetrieveSerializer()
    financial_info = BusinessFinancialRetrieveSerializer()
    additional_info = BusinessAdditionalRetrieveSerializer()
    contact_info = BusinessContactRetrieveSerializer()
    recommendation = serializers.SerializerMethodField("get_recommendation")

    class Meta:
        model = Business
        fields = ('pk', 'advertisement', 'basic_info', 'financial_info', 'additional_info', 'contact_info', 'recommendation')

    def get_recommendation(self, obj):
        queryset_all = Business.objects \
            .exclude(id__in=[obj.id]) \
            .filter(advertisement__status__in=['ACCEPTED'])

        queryset_category = queryset_all.filter(basic_info__category_id__in=[obj.basic_info.category_id]) \
                                .order_by('-advertisement__created_at')[:3]

        if len(queryset_category) == 3:
            return BusinessListSerializer(queryset_category, many=True, context={'request': self.context.get('request')}).data
        else:
            queryset_left = queryset_all.exclude(id__in=queryset_category.values_list('id')) \
                                .order_by('-advertisement__created_at')[:3 - len(queryset_category)]
            return BusinessListSerializer(queryset_category, many=True, context={'request': self.context.get('request')}).data \
                   + BusinessListSerializer(queryset_left, many=True, context={'request': self.context.get('request')}).data


class BusinessListSerializer(serializers.ModelSerializer):
    advertisement = AdvertisementRetrieveSerializer()
    basic_info = BusinessBasicRetrieveSerializer()
    financial_info = BusinessFinancialRetrieveSerializer()
    additional_info = BusinessAdditionalRetrieveSerializer()
    contact_info = BusinessContactRetrieveSerializer()

    class Meta:
        model = Business
        fields = ('pk', 'advertisement', 'basic_info', 'financial_info', 'additional_info', 'contact_info')


class BusinessSerializer(serializers.ModelSerializer):
    basic_info = BusinessBasicSerializer()
    financial_info = BusinessFinancialSerializer()
    additional_info = BusinessAdditionalSerializer()
    contact_info = BusinessContactSerializer()

    class Meta:
        model = Business
        fields = ('basic_info', 'financial_info', 'additional_info', 'contact_info')

    @transaction.atomic
    def create(self, validated_data):
        validated_data.pop('basic_info')
        validated_data.pop('financial_info')
        validated_data.pop('additional_info')
        validated_data.pop('contact_info')

        # Advertisement
        advertisement_data = {
            "type": 1
        }
        advertisement = AdvertisementSerializer(data=advertisement_data, context={'request': self.context.get('request')})
        assert advertisement.is_valid(raise_exception=True)
        advertisement = advertisement.save()

        # Business
        validated_data['advertisement'] = advertisement
        business = Business.objects.create(**validated_data)

        # Basic info
        basic_info_data = self.initial_data.pop('basic_info')
        basic_info = BusinessBasicSerializer(data=basic_info_data)
        assert basic_info.is_valid(raise_exception=True)
        basic_info.save(business=business)

        # Financial info
        financial_info_data = self.initial_data.pop('financial_info')
        financial_info = BusinessFinancialSerializer(data=financial_info_data)
        assert financial_info.is_valid(raise_exception=True)
        financial_info.save(business=business)

        # Additional info
        additional_info_data = self.initial_data.pop('additional_info')
        additional_info = BusinessAdditionalSerializer(data=additional_info_data)
        assert additional_info.is_valid(raise_exception=True)
        additional_info.save(business=business)

        # Contact info
        contact_info_data = self.initial_data.pop('contact_info')
        contact_info = BusinessContactSerializer(data=contact_info_data)
        assert contact_info.is_valid(raise_exception=True)
        contact_info.save(business=business)

        return business


class BusinessUpdateSerializer(serializers.ModelSerializer):
    basic_info = BusinessBasicUpdateSerializer()
    financial_info = BusinessFinancialUpdateSerializer()
    additional_info = BusinessAdditionalUpdateSerializer()
    contact_info = BusinessContactUpdateSerializer()

    class Meta:
        model = Business
        fields = ('basic_info', 'financial_info', 'additional_info', 'contact_info')

    def update(self, instance, validated_data):
        basic_info_data = self.initial_data.pop('basic_info')
        financial_info_data = self.initial_data.pop('financial_info')
        additional_info_data = self.initial_data.pop('additional_info')
        contact_info_data = self.initial_data.pop('contact_info')

        basic_info = instance.basic_info
        financial_info = instance.financial_info
        additional_info = instance.additional_info
        contact_info = instance.contact_info

        # Business
        advertisement = instance.advertisement
        advertisement.status = 'UPDATED'
        advertisement.save()

        # Basic info
        basic_info = BusinessBasicUpdateSerializer(basic_info, data=basic_info_data)
        assert basic_info.is_valid(raise_exception=True)
        basic_info.save()

        # Financial info
        financial_info = BusinessFinancialUpdateSerializer(financial_info, data=financial_info_data)
        assert financial_info.is_valid(raise_exception=True)
        financial_info.save()

        # Additional info
        additional_info = BusinessAdditionalUpdateSerializer(additional_info, data=additional_info_data)
        assert additional_info.is_valid(raise_exception=True)
        additional_info.save()

        # Contact info
        contact_info = BusinessContactUpdateSerializer(contact_info, data=contact_info_data)
        assert contact_info.is_valid(raise_exception=True)
        contact_info.save()

        return instance
