# Generated by Django 3.1.6 on 2021-03-11 18:24

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('advertisements', '0002_auto_20210311_1824'),
        ('core', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Business',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('advertisement', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='businesses', to='advertisements.advertisement')),
            ],
            options={
                'verbose_name': 'business',
                'verbose_name_plural': 'businesses',
                'db_table': 'api_businesses_business',
            },
        ),
        migrations.CreateModel(
            name='BusinessAdditional',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('buildings', models.IntegerField(choices=[(1, 'В аренде'), (2, 'В собственности'), (3, 'Не требуется')])),
                ('staff_count', models.IntegerField(blank=True, null=True)),
                ('production_means', models.CharField(blank=True, max_length=512, null=True)),
                ('business', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='additional_info', to='businesses.business')),
            ],
            options={
                'verbose_name': 'business additional',
                'verbose_name_plural': 'business additional',
                'db_table': 'api_businesses_business_additional',
            },
        ),
        migrations.CreateModel(
            name='BusinessBasic',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('name', models.CharField(max_length=256)),
                ('price', models.IntegerField()),
                ('years_on_market', models.IntegerField()),
                ('business', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='basic_info', to='businesses.business')),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='core.category')),
                ('city', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='core.city')),
            ],
            options={
                'verbose_name': 'business basic',
                'verbose_name_plural': 'business basic',
                'db_table': 'api_businesses_business_basic',
            },
        ),
        migrations.CreateModel(
            name='BusinessFinancial',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('earnings', models.IntegerField()),
                ('spending', models.IntegerField()),
                ('share', models.IntegerField()),
                ('borrowed_funds', models.CharField(blank=True, max_length=256, null=True)),
                ('goods_remainder', models.CharField(blank=True, max_length=256, null=True)),
                ('estimated_payback_year', models.IntegerField()),
                ('estimated_payback_month', models.IntegerField()),
                ('business', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='financial_info', to='businesses.business')),
            ],
            options={
                'verbose_name': 'business financial',
                'verbose_name_plural': 'business financials',
                'db_table': 'api_businesses_business_financial',
            },
        ),
        migrations.CreateModel(
            name='BusinessContact',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('contact', models.CharField(max_length=128)),
                ('email', models.EmailField(max_length=128)),
                ('phone', models.CharField(max_length=11, validators=[django.core.validators.RegexValidator(message="Phone number must be entered in the format: '77077070077'. Up to 11 digits allowed.", regex='^(7)([0-9]{10})$')])),
                ('business_description', models.CharField(max_length=512)),
                ('business', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='contact_info', to='businesses.business')),
            ],
            options={
                'verbose_name': 'business contact',
                'verbose_name_plural': 'business contacts',
                'db_table': 'api_businesses_business_contact',
            },
        ),
        migrations.CreateModel(
            name='BusinessBasicMedia',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('image', models.ImageField(upload_to='media/')),
                ('business_basic', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='media', to='businesses.businessbasic')),
            ],
            options={
                'verbose_name': 'business basic media',
                'verbose_name_plural': 'business basic media',
                'db_table': 'api_businesses_business_basic_media',
            },
        ),
        migrations.CreateModel(
            name='BusinessAdditionalDocument',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('file', models.FileField(upload_to='documents/')),
                ('business_additional', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='documents', to='businesses.businessadditional')),
            ],
            options={
                'verbose_name': 'business additional document',
                'verbose_name_plural': 'business additional documents',
                'db_table': 'api_businesses_business_additional_document',
            },
        ),
    ]
