import json

from django.db.models import F
from rest_framework import status, mixins
from rest_framework.generics import UpdateAPIView
from rest_framework.parsers import FileUploadParser
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, GenericViewSet

from adax.paginations import SmallResultSetPagination
from adax.permissions import IsAdmin, IsConfirmedUser
from apps.advertisements.models import Advertisement
from apps.advertisements.tasks import advertisement_views_notification
from apps.businesses.models import Business, BusinessBasic, BusinessAdditional
from apps.businesses.serializers import BusinessSerializer, BusinessRetrieveSerializer, BusinessListSerializer, \
    BusinessUpdateSerializer, BusinessBasicMediaUploadSerializer, BusinessBasicRetrieveSerializer, \
    BusinessBasicMediaDeleteSerializer, BusinessAdditionalRetrieveSerializer, \
    BusinessAdditionalDocumentsDeleteSerializer, BusinessAdditionalDocumentsUploadSerializer, \
    BusinessSearchRetrieveSerializer


class BusinessViewSet(ModelViewSet):
    queryset = Business.objects.all()
    permission_classes = (IsAuthenticated, IsConfirmedUser,)
    serializer_class = BusinessSerializer
    pagination_class = SmallResultSetPagination

    def get_serializer_class(self):
        if self.action in ['create']:
            return BusinessSerializer
        elif self.action in ['update']:
            return BusinessUpdateSerializer
        elif self.action in ['list']:
            return BusinessListSerializer
        return BusinessRetrieveSerializer

    def get_queryset(self):
        owner_queryset = self.queryset.filter(advertisement__user=self.request.user.id)
        if self.action in ['update']:
            owner_queryset = owner_queryset.exclude(advertisement__status__in=['ARCHIVED'])

        return owner_queryset

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        business = self.perform_create(serializer)

        read_serializer = BusinessRetrieveSerializer(business)
        headers = self.get_success_headers(read_serializer.data)
        return Response(read_serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        return serializer.save()

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        advertisement = self.perform_update(serializer)
        read_serializer = BusinessRetrieveSerializer(advertisement)

        headers = self.get_success_headers(read_serializer.data)
        return Response(read_serializer.data, status=status.HTTP_200_OK, headers=headers)

    def perform_update(self, serializer):
        return serializer.save()


class BusinessBasicMediaUploadViewSet(UpdateAPIView):
    lookup_field = 'pk'
    permission_classes = (IsAuthenticated, IsConfirmedUser,)
    queryset = BusinessBasic.objects.all()
    serializer_class = BusinessBasicMediaUploadSerializer
    parser_class = (FileUploadParser,)

    def get_queryset(self):
        owner_queryset = self.queryset.filter(business__advertisement__user=self.request.user.id) \
            .exclude(business__advertisement__status__in=['ARCHIVED'])

        return owner_queryset

    def update(self, request, *args, **kwargs):
        instance = self.get_object()

        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        basic_info = self.perform_update(serializer)

        read_serializer = BusinessBasicRetrieveSerializer(basic_info)
        return Response(read_serializer.data, status=status.HTTP_200_OK)

    def perform_update(self, serializer):
        return serializer.save()


class BusinessBasicMediaDeleteViewSet(UpdateAPIView):
    lookup_field = 'pk'
    permission_classes = (IsAuthenticated, IsConfirmedUser,)
    queryset = BusinessBasic.objects.all()
    serializer_class = BusinessBasicMediaDeleteSerializer

    def get_queryset(self):
        owner_queryset = self.queryset.filter(business__advertisement__user=self.request.user.id) \
            .exclude(business__advertisement__status__in=['ARCHIVED'])

        return owner_queryset

    def update(self, request, *args, **kwargs):
        instance = self.get_object()

        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        basic_info = self.perform_update(serializer)

        read_serializer = BusinessBasicRetrieveSerializer(basic_info)
        return Response(read_serializer.data, status=status.HTTP_200_OK)

    def perform_update(self, serializer):
        return serializer.save()


class BusinessAdditionalDocumentsUploadViewSet(UpdateAPIView):
    lookup_field = 'pk'
    permission_classes = (IsAuthenticated, IsConfirmedUser,)
    queryset = BusinessAdditional.objects.all()
    serializer_class = BusinessAdditionalDocumentsUploadSerializer
    parser_class = (FileUploadParser,)

    def get_queryset(self):
        owner_queryset = self.queryset.filter(business__advertisement__user=self.request.user.id) \
            .exclude(business__advertisement__status__in=['ARCHIVED'])

        return owner_queryset

    def update(self, request, *args, **kwargs):
        instance = self.get_object()

        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        basic_info = self.perform_update(serializer)

        read_serializer = BusinessAdditionalRetrieveSerializer(basic_info)
        return Response(read_serializer.data, status=status.HTTP_200_OK)

    def perform_update(self, serializer):
        return serializer.save()


class BusinessAdditionalDocumentsDeleteViewSet(UpdateAPIView):
    lookup_field = 'pk'
    permission_classes = (IsAuthenticated, IsConfirmedUser,)
    queryset = BusinessAdditional.objects.all()
    serializer_class = BusinessAdditionalDocumentsDeleteSerializer

    def get_queryset(self):
        owner_queryset = self.queryset.filter(business__advertisement__user=self.request.user.id) \
            .exclude(business__advertisement__status__in=['ARCHIVED'])

        return owner_queryset

    def update(self, request, *args, **kwargs):
        instance = self.get_object()

        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        basic_info = self.perform_update(serializer)

        read_serializer = BusinessAdditionalRetrieveSerializer(basic_info)
        return Response(read_serializer.data, status=status.HTTP_200_OK)

    def perform_update(self, serializer):
        return serializer.save()


class BusinessAdminViewSet(GenericViewSet, mixins.ListModelMixin):
    queryset = Business.objects.all()
    permission_classes = (IsAuthenticated, IsAdmin,)
    serializer_class = BusinessListSerializer
    pagination_class = SmallResultSetPagination


class BusinessSearchViewSet(GenericViewSet, mixins.ListModelMixin, mixins.RetrieveModelMixin):
    queryset = Business.objects.filter(advertisement__status__in=['ACCEPTED'])
    permission_classes = (AllowAny,)
    serializer_class = BusinessSearchRetrieveSerializer
    pagination_class = SmallResultSetPagination

    def get_serializer_class(self):
        if self.action in ['list']:
            return BusinessListSerializer
        return BusinessSearchRetrieveSerializer

    def get_queryset(self):
        queryset = self.queryset
        city = self.request.query_params.get('city')
        category = json.loads(self.request.query_params.get('category', '[]'))
        start_price = self.request.query_params.get('start_price')
        end_price = self.request.query_params.get('end_price')
        name = self.request.query_params.get('name')
        tariff_name = self.request.query_params.get('tariff_name')
        sort_type = self.request.query_params.get('sort_type')

        if city:
            queryset = queryset.filter(basic_info__city_id__in=city)
        if category:
            queryset = queryset.filter(basic_info__category_id__in=category)
        if start_price:
            queryset = queryset.filter(basic_info__price__gt=start_price)
        if end_price:
            queryset = queryset.filter(basic_info__price__lt=end_price)
        if name:
            queryset = queryset.filter(basic_info__name__icontains=name)
        if tariff_name:
            queryset = queryset.filter(advertisement__tariff_name=tariff_name)
        if sort_type:
            if sort_type == 'date_desc':
                return queryset.order_by('-advertisement__created_at')
            elif sort_type == 'date_asc':
                return queryset.order_by('advertisement__created_at')
            elif sort_type == 'price_desc':
                return queryset.order_by('-basic_info__price')
            elif sort_type == 'price_asc':
                return queryset.order_by('basic_info__price')

        return queryset.order_by('-advertisement__created_at')

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        Advertisement.objects.filter(businesses=instance).update(view_count=F('view_count')+1)
        if instance.advertisement.view_count > 0 and instance.advertisement.view_count % 10 == 0:
            advertisement_views_notification(instance.advertisement.user)
        return Response(serializer.data)
