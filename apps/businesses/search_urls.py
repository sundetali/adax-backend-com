from django.urls import path, include
from rest_framework import routers

from apps.businesses.views import BusinessViewSet, BusinessSearchViewSet

app_name = "businesses"

router = routers.DefaultRouter()
router.register(r'', BusinessSearchViewSet, basename='business_search')

urlpatterns = [
    path('', include(router.urls)),
]
