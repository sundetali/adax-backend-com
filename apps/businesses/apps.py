from django.apps import AppConfig


class BusinessesConfig(AppConfig):
    name = 'apps.businesses'
    verbose_name = 'Business'
