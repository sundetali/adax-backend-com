from django.apps import AppConfig


class FranchisesConfig(AppConfig):
    name = 'apps.franchises'
    verbose_name = 'Franchise'
