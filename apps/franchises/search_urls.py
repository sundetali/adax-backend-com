from django.urls import path, include
from rest_framework import routers

from apps.franchises.views import FranchiseViewSet, FranchiseSearchViewSet

app_name = "franchises"

router = routers.DefaultRouter()
router.register(r'', FranchiseSearchViewSet, basename='franchise_search')

urlpatterns = [
    path('', include(router.urls)),
]
