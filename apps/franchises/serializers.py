from rest_framework import serializers

from apps.advertisements.serializers import AdvertisementSerializer, AdvertisementRetrieveSerializer
from apps.core.serializers import CategoryRetrieveSerializer
from apps.franchises.models import FranchiseBasic, FranchiseAdditional, FranchiseRequirement, \
    FranchiseContact, Franchise, FranchiseRequirementMedia, FranchiseRequirementDocument


class FranchiseBasicRetrieveSerializer(serializers.ModelSerializer):
    category = CategoryRetrieveSerializer()

    class Meta:
        model = FranchiseBasic
        fields = ('pk', 'name', 'category', 'foundation_year', 'launch_year', 'sold_franchises_count',
                  'own_franchises_count', 'initial_expenses')


class FranchiseBasicSerializer(serializers.ModelSerializer):
    class Meta:
        model = FranchiseBasic
        fields = ('name', 'category', 'foundation_year', 'launch_year', 'sold_franchises_count', 'own_franchises_count',
                  'initial_expenses')


class FranchiseBasicUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = FranchiseBasic
        fields = ('name', 'category', 'foundation_year', 'launch_year', 'sold_franchises_count', 'own_franchises_count',
                  'initial_expenses')


class FranchiseAdditionalRetrieveSerializer(serializers.ModelSerializer):
    class Meta:
        model = FranchiseAdditional
        fields = ('pk', 'initial_fee', 'royalty', 'royalty_info', 'current_payments', 'is_tutorial_exist',
                  'short_description')


class FranchiseAdditionalSerializer(serializers.ModelSerializer):
    class Meta:
        model = FranchiseAdditional
        fields = (
            'initial_fee', 'royalty', 'royalty_info', 'current_payments', 'is_tutorial_exist', 'short_description')


class FranchiseAdditionalUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = FranchiseAdditional
        fields = (
            'initial_fee', 'royalty', 'royalty_info', 'current_payments', 'is_tutorial_exist', 'short_description')


class FranchiseRequirementMediaSerializer(serializers.ModelSerializer):
    class Meta:
        model = FranchiseRequirementMedia
        fields = ('image',)


class FranchiseRequirementMediaRetrieveSerializer(serializers.ModelSerializer):
    class Meta:
        model = FranchiseRequirementMedia
        fields = ('id', 'image',)


class FranchiseRequirementMediaUploadSerializer(serializers.ModelSerializer):
    media = serializers.ListField(child=serializers.FileField())

    class Meta:
        model = FranchiseRequirement
        fields = ('media',)

    def update(self, instance, validated_data):
        media_data_list = validated_data.get('media', [])

        for media in media_data_list:
            new_media = {
                'image': media
            }
            document = FranchiseRequirementMediaSerializer(data=new_media)
            assert document.is_valid(raise_exception=True)
            document.save(franchise_requirement=instance)

        return instance


class FranchiseRequirementMediaDeleteSerializer(serializers.ModelSerializer):
    media_to_delete = serializers.ListField(child=serializers.IntegerField())

    class Meta:
        model = FranchiseRequirement
        fields = ('media_to_delete',)

    def update(self, instance, validated_data):
        ids_to_delete = validated_data.get('media_to_delete', [])

        for id_to_delete in ids_to_delete:
            item = instance.media.filter(id=id_to_delete)

            if item.exists():
                item.delete()

        return instance


class FranchiseRequirementDocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = FranchiseRequirementDocument
        fields = ('file',)


class FranchiseRequirementDocumentRetrieveSerializer(serializers.ModelSerializer):
    class Meta:
        model = FranchiseRequirementDocument
        fields = ('id', 'file',)


class FranchiseRequirementDocumentsUploadSerializer(serializers.ModelSerializer):
    documents = serializers.ListField(child=serializers.FileField())

    class Meta:
        model = FranchiseRequirement
        fields = ('documents',)

    def update(self, instance, validated_data):
        documents_data_list = validated_data.get('documents', [])

        for document in documents_data_list:
            new_document = {
                'file': document
            }
            document = FranchiseRequirementDocumentSerializer(data=new_document)
            assert document.is_valid(raise_exception=True)
            document.save(franchise_requirement=instance)

        return instance


class FranchiseRequirementDocumentsDeleteSerializer(serializers.ModelSerializer):
    documents_to_delete = serializers.ListField(child=serializers.IntegerField())

    class Meta:
        model = FranchiseRequirement
        fields = ('documents_to_delete',)

    def update(self, instance, validated_data):
        ids_to_delete = validated_data.get('documents_to_delete', [])

        for id_to_delete in ids_to_delete:
            item = instance.documents.filter(id=id_to_delete)

            if item.exists():
                item.delete()

        return instance


class FranchiseRequirementRetrieveSerializer(serializers.ModelSerializer):
    media = FranchiseRequirementMediaRetrieveSerializer(many=True)
    documents = FranchiseRequirementDocumentRetrieveSerializer(many=True)

    class Meta:
        model = FranchiseRequirement
        fields = ('pk', 'package_content', 'franchisee_requirement', 'media', 'documents')


class FranchiseRequirementSerializer(serializers.ModelSerializer):
    class Meta:
        model = FranchiseRequirement
        fields = ('package_content', 'franchisee_requirement')

    def create(self, validated_data):
        # Franchise Requirement
        franchise_requirement = FranchiseRequirement.objects.create(**validated_data)

        return franchise_requirement


class FranchiseRequirementUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = FranchiseRequirement
        fields = ('package_content', 'franchisee_requirement')

    def update(self, instance, validated_data):
        # Franchise Requirement
        instance.package_content = validated_data.get('package_content', instance.package_content)
        instance.franchisee_requirement = validated_data.get('franchisee_requirement', instance.franchisee_requirement)

        instance.save()

        return instance


class FranchiseContactRetrieveSerializer(serializers.ModelSerializer):
    class Meta:
        model = FranchiseContact
        fields = ('pk', 'contact', 'email', 'phone')


class FranchiseContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = FranchiseContact
        fields = ('contact', 'email', 'phone')


class FranchiseContactUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = FranchiseContact
        fields = ('contact', 'email', 'phone')


class FranchiseRetrieveSerializer(serializers.ModelSerializer):
    advertisement = AdvertisementRetrieveSerializer()
    basic_info = FranchiseBasicRetrieveSerializer()
    additional_info = FranchiseAdditionalRetrieveSerializer()
    requirement_info = FranchiseRequirementRetrieveSerializer()
    contact_info = FranchiseContactRetrieveSerializer()

    class Meta:
        model = Franchise
        fields = ('pk', 'advertisement', 'basic_info', 'additional_info', 'requirement_info', 'contact_info')


class FranchiseSearchRetrieveSerializer(serializers.ModelSerializer):
    advertisement = AdvertisementRetrieveSerializer()
    basic_info = FranchiseBasicRetrieveSerializer()
    additional_info = FranchiseAdditionalRetrieveSerializer()
    requirement_info = FranchiseRequirementRetrieveSerializer()
    contact_info = FranchiseContactRetrieveSerializer()
    recommendation = serializers.SerializerMethodField("get_recommendation")

    class Meta:
        model = Franchise
        fields = (
            'pk', 'advertisement', 'basic_info', 'additional_info', 'requirement_info', 'contact_info',
            'recommendation')

    def get_recommendation(self, obj):
        queryset_all = Franchise.objects \
            .exclude(id__in=[obj.id]) \
            .filter(advertisement__status__in=['ACCEPTED'])

        queryset_category = queryset_all.filter(basic_info__category_id__in=[obj.basic_info.category_id]) \
                                .order_by('-advertisement__created_at')[:3]

        if len(queryset_category) == 3:
            return FranchiseListSerializer(queryset_category, many=True, context={'request': self.context.get('request')}).data
        else:
            queryset_left = queryset_all.exclude(id__in=queryset_category.values_list('id')) \
                                .order_by('-advertisement__created_at')[:3 - len(queryset_category)]
            return FranchiseListSerializer(queryset_category, many=True, context={'request': self.context.get('request')}).data \
                   + FranchiseListSerializer(queryset_left, many=True, context={'request': self.context.get('request')}).data


class FranchiseListSerializer(serializers.ModelSerializer):
    advertisement = AdvertisementRetrieveSerializer()
    basic_info = FranchiseBasicRetrieveSerializer()
    additional_info = FranchiseAdditionalRetrieveSerializer()
    requirement_info = FranchiseRequirementRetrieveSerializer()
    contact_info = FranchiseContactRetrieveSerializer()

    class Meta:
        model = Franchise
        fields = ('pk', 'advertisement', 'basic_info', 'additional_info', 'requirement_info', 'contact_info')


class FranchiseSerializer(serializers.ModelSerializer):
    basic_info = FranchiseBasicSerializer()
    additional_info = FranchiseAdditionalSerializer()
    requirement_info = FranchiseRequirementSerializer()
    contact_info = FranchiseContactSerializer()

    class Meta:
        model = Franchise
        fields = ('basic_info', 'additional_info', 'requirement_info', 'contact_info')

    def create(self, validated_data):
        validated_data.pop('basic_info')
        validated_data.pop('additional_info')
        validated_data.pop('requirement_info')
        validated_data.pop('contact_info')

        # Advertisement
        advertisement_data = {
            "type": 2
        }
        advertisement = AdvertisementSerializer(data=advertisement_data,
                                                context={'request': self.context.get('request')})
        assert advertisement.is_valid(raise_exception=True)
        advertisement = advertisement.save()

        # Franchise
        validated_data['advertisement'] = advertisement
        franchise = Franchise.objects.create(**validated_data)

        # Basic info
        basic_info_data = self.initial_data.pop('basic_info')
        basic_info = FranchiseBasicSerializer(data=basic_info_data)
        assert basic_info.is_valid(raise_exception=True)
        basic_info.save(franchise=franchise)

        # Additional info
        additional_info_data = self.initial_data.pop('additional_info')
        additional_info = FranchiseAdditionalSerializer(data=additional_info_data)
        assert additional_info.is_valid(raise_exception=True)
        additional_info.save(franchise=franchise)

        # Requirement info
        requirement_info_data = self.initial_data.pop('requirement_info')
        requirement_info = FranchiseRequirementSerializer(data=requirement_info_data)
        assert requirement_info.is_valid(raise_exception=True)
        requirement_info.save(franchise=franchise)

        # Contact info
        contact_info_data = self.initial_data.pop('contact_info')
        contact_info = FranchiseContactSerializer(data=contact_info_data)
        assert contact_info.is_valid(raise_exception=True)
        contact_info.save(franchise=franchise)

        return franchise


class FranchiseUpdateSerializer(serializers.ModelSerializer):
    basic_info = FranchiseBasicUpdateSerializer()
    additional_info = FranchiseAdditionalUpdateSerializer()
    requirement_info = FranchiseRequirementUpdateSerializer()
    contact_info = FranchiseContactUpdateSerializer()

    class Meta:
        model = Franchise
        fields = ('basic_info', 'additional_info', 'requirement_info', 'contact_info')

    def update(self, instance, validated_data):
        basic_info_data = self.initial_data.pop('basic_info')
        additional_info_data = self.initial_data.pop('additional_info')
        requirement_info_data = self.initial_data.pop('requirement_info')
        contact_info_data = self.initial_data.pop('contact_info')

        basic_info = instance.basic_info
        additional_info = instance.additional_info
        requirement_info = instance.requirement_info
        contact_info = instance.contact_info

        # Franchise
        advertisement = instance.advertisement
        advertisement.status = 'UPDATED'
        advertisement.save()

        # Basic info
        basic_info = FranchiseBasicUpdateSerializer(basic_info, data=basic_info_data)
        assert basic_info.is_valid(raise_exception=True)
        basic_info.save()

        # Additional info
        additional_info = FranchiseAdditionalUpdateSerializer(additional_info, data=additional_info_data)
        assert additional_info.is_valid(raise_exception=True)
        additional_info.save()

        # Requirement info
        requirement_info = FranchiseRequirementUpdateSerializer(requirement_info, data=requirement_info_data)
        assert requirement_info.is_valid(raise_exception=True)
        requirement_info.save()

        # Contact info
        contact_info = FranchiseContactUpdateSerializer(contact_info, data=contact_info_data)
        assert contact_info.is_valid(raise_exception=True)
        contact_info.save()

        return instance
