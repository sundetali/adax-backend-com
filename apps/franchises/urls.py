from django.urls import path, include
from rest_framework import routers

from apps.franchises.views import FranchiseViewSet, FranchiseAdminViewSet, FranchiseRequirementMediaUploadViewSet, \
    FranchiseRequirementMediaDeleteViewSet, FranchiseRequirementDocumentsUploadViewSet, \
    FranchiseRequirementDocumentsDeleteViewSet

app_name = "franchises"

admin_router = routers.DefaultRouter()
admin_router.register(r'', FranchiseAdminViewSet, basename='franchise_admin')

router = routers.DefaultRouter()
router.register(r'', FranchiseViewSet, basename='franchise')

urlpatterns = [
    path('requirement-info/<int:pk>/upload-media/', FranchiseRequirementMediaUploadViewSet.as_view(),
         name='franchise_requirement_media_upload'),
    path('requirement-info/<int:pk>/delete-media/', FranchiseRequirementMediaDeleteViewSet.as_view(),
         name='franchise_requirement_media_delete'),
    path('requirement-info/<int:pk>/upload-documents/', FranchiseRequirementDocumentsUploadViewSet.as_view(),
         name='franchise_requirement_documents_upload'),
    path('requirement-info/<int:pk>/delete-documents/', FranchiseRequirementDocumentsDeleteViewSet.as_view(),
         name='franchise_requirement_documents_delete'),
    path('admin/', include(admin_router.urls)),
    path('', include(router.urls)),
]
