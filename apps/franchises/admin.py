from django.contrib import admin

from apps.franchises.models import FranchiseBasic, FranchiseAdditional, FranchiseRequirement, \
    FranchiseContact, Franchise, FranchiseRequirementMedia, FranchiseRequirementDocument

admin.site.register(
    [
        FranchiseBasic,
        FranchiseAdditional,
        FranchiseRequirement,
        FranchiseRequirementMedia,
        FranchiseRequirementDocument,
        FranchiseContact,
        Franchise
    ]
)
