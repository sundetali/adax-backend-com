import os

from django.core.validators import RegexValidator
from django.db import models
from django.dispatch import receiver
from safedelete.models import SafeDeleteModel

from apps.advertisements.models import Advertisement
from apps.core.models import Category


class Franchise(SafeDeleteModel):
    advertisement = models.OneToOneField(Advertisement, related_name='franchises', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.basic_info)

    class Meta:
        db_table = 'api_franchises_franchise'
        verbose_name = 'franchise'
        verbose_name_plural = 'franchises'


class FranchiseBasic(SafeDeleteModel):
    name = models.CharField(max_length=1024)
    category = models.ForeignKey(Category, on_delete=models.DO_NOTHING)
    foundation_year = models.IntegerField()
    launch_year = models.IntegerField()
    sold_franchises_count = models.IntegerField(blank=True, null=True)
    own_franchises_count = models.IntegerField()
    initial_expenses = models.IntegerField()
    franchise = models.OneToOneField(Franchise, related_name='basic_info', on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'api_franchises_franchise_basic'
        verbose_name = 'franchise basic'
        verbose_name_plural = 'franchise basic'


class FranchiseAdditional(SafeDeleteModel):
    initial_fee = models.IntegerField()
    royalty = models.IntegerField()
    royalty_info = models.CharField(max_length=2048, null=True, blank=True)
    current_payments = models.CharField(max_length=2048, null=True, blank=True)
    is_tutorial_exist = models.BooleanField()
    short_description = models.CharField(max_length=2048)
    franchise = models.OneToOneField(Franchise, related_name='additional_info', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.franchise) if hasattr(self, 'franchise') else str(self.id)

    class Meta:
        db_table = 'api_franchises_franchise_additional'
        verbose_name = 'franchise additional'
        verbose_name_plural = 'franchise additional'


class FranchiseRequirement(SafeDeleteModel):
    package_content = models.CharField(max_length=2048)
    franchisee_requirement = models.CharField(max_length=2048)
    franchise = models.OneToOneField(Franchise, related_name='requirement_info', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.franchise) if hasattr(self, 'franchise') else str(self.id)

    class Meta:
        db_table = 'api_franchises_franchise_requirement'
        verbose_name = 'franchise requirement'
        verbose_name_plural = 'franchise requirements'


class FranchiseRequirementMedia(SafeDeleteModel):
    image = models.ImageField(upload_to='media/')
    franchise_requirement = models.ForeignKey(FranchiseRequirement, on_delete=models.CASCADE, related_name='media')

    def __str__(self):
        return str(self.franchise_requirement)

    class Meta:
        db_table = 'api_franchises_franchise_requirement_media'
        verbose_name = 'franchise requirement media'
        verbose_name_plural = 'franchise requirement medias'


@receiver(models.signals.post_delete, sender=FranchiseRequirementMedia)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    if instance.image:
        if os.path.isfile(instance.image.path):
            os.remove(instance.image.path)


class FranchiseRequirementDocument(SafeDeleteModel):
    file = models.FileField(upload_to='documents/')
    franchise_requirement = models.ForeignKey(FranchiseRequirement, on_delete=models.CASCADE, related_name='documents')

    def __str__(self):
        return str(self.franchise_requirement)

    class Meta:
        db_table = 'api_franchises_franchise_requirement_document'
        verbose_name = 'franchise requirement document'
        verbose_name_plural = 'franchise requirement documents'


@receiver(models.signals.post_delete, sender=FranchiseRequirementDocument)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    if instance.file:
        if os.path.isfile(instance.file.path):
            os.remove(instance.file.path)


class FranchiseContact(SafeDeleteModel):
    contact = models.CharField(max_length=512)
    email = models.EmailField(max_length=512)
    phone_regex = RegexValidator(regex=r'^(7)([0-9]{10})$',
                                 message="Phone number must be entered in the format: '77077070077'. Up to 11 digits "
                                         "allowed.")
    phone = models.CharField(validators=[phone_regex], max_length=11)
    franchise = models.OneToOneField(Franchise, related_name='contact_info', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.franchise) if hasattr(self, 'franchise') else str(self.id)

    class Meta:
        db_table = 'api_franchises_franchise_contact'
        verbose_name = 'franchise contact'
        verbose_name_plural = 'franchise contacts'
