import json

from django.db.models import F
from rest_framework import status, mixins
from rest_framework.generics import UpdateAPIView
from rest_framework.parsers import FileUploadParser
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, GenericViewSet

from adax.paginations import SmallResultSetPagination
from adax.permissions import IsAdmin, IsConfirmedUser
from apps.advertisements.models import Advertisement
from apps.advertisements.tasks import advertisement_views_notification
from apps.franchises.models import Franchise, FranchiseRequirement
from apps.franchises.serializers import FranchiseSerializer, FranchiseRetrieveSerializer, FranchiseListSerializer, \
    FranchiseUpdateSerializer, FranchiseRequirementRetrieveSerializer, FranchiseRequirementMediaUploadSerializer, \
    FranchiseRequirementMediaDeleteSerializer, FranchiseRequirementDocumentsUploadSerializer, \
    FranchiseRequirementDocumentsDeleteSerializer, FranchiseSearchRetrieveSerializer


class FranchiseViewSet(ModelViewSet):
    queryset = Franchise.objects.all()
    permission_classes = (IsAuthenticated, IsConfirmedUser,)
    serializer_class = FranchiseSerializer
    pagination_class = SmallResultSetPagination

    def get_serializer_class(self):
        if self.action in ['create']:
            return FranchiseSerializer
        elif self.action in ['update']:
            return FranchiseUpdateSerializer
        elif self.action in ['list']:
            return FranchiseListSerializer
        return FranchiseRetrieveSerializer

    def get_queryset(self):
        owner_queryset = self.queryset.filter(advertisement__user=self.request.user.id)
        if self.action in ['update']:
            owner_queryset = owner_queryset.exclude(advertisement__status__in=['ARCHIVED'])

        return owner_queryset

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        business = self.perform_create(serializer)

        read_serializer = FranchiseRetrieveSerializer(business)
        headers = self.get_success_headers(read_serializer.data)
        return Response(read_serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        return serializer.save()

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        advertisement = self.perform_update(serializer)
        read_serializer = FranchiseRetrieveSerializer(advertisement)

        headers = self.get_success_headers(read_serializer.data)
        return Response(read_serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_update(self, serializer):
        return serializer.save()


class FranchiseRequirementMediaUploadViewSet(UpdateAPIView):
    lookup_field = 'pk'
    permission_classes = (IsAuthenticated, IsConfirmedUser,)
    queryset = FranchiseRequirement.objects.all()
    serializer_class = FranchiseRequirementMediaUploadSerializer
    parser_class = (FileUploadParser,)

    def get_queryset(self):
        owner_queryset = self.queryset.filter(franchise__advertisement__user=self.request.user.id) \
            .exclude(franchise__advertisement__status__in=['ARCHIVED'])

        return owner_queryset

    def update(self, request, *args, **kwargs):
        instance = self.get_object()

        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        requirement_info = self.perform_update(serializer)

        read_serializer = FranchiseRequirementRetrieveSerializer(requirement_info)
        return Response(read_serializer.data, status=status.HTTP_200_OK)

    def perform_update(self, serializer):
        return serializer.save()


class FranchiseRequirementMediaDeleteViewSet(UpdateAPIView):
    lookup_field = 'pk'
    permission_classes = (IsAuthenticated, IsConfirmedUser,)
    queryset = FranchiseRequirement.objects.all()
    serializer_class = FranchiseRequirementMediaDeleteSerializer

    def get_queryset(self):
        owner_queryset = self.queryset.filter(franchise__advertisement__user=self.request.user.id) \
            .exclude(franchise__advertisement__status__in=['ARCHIVED'])

        return owner_queryset

    def update(self, request, *args, **kwargs):
        instance = self.get_object()

        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        requirement_info = self.perform_update(serializer)

        read_serializer = FranchiseRequirementRetrieveSerializer(requirement_info)
        return Response(read_serializer.data, status=status.HTTP_200_OK)

    def perform_update(self, serializer):
        return serializer.save()


class FranchiseRequirementDocumentsUploadViewSet(UpdateAPIView):
    lookup_field = 'pk'
    permission_classes = (IsAuthenticated, IsConfirmedUser,)
    queryset = FranchiseRequirement.objects.all()
    serializer_class = FranchiseRequirementDocumentsUploadSerializer
    parser_class = (FileUploadParser,)

    def get_queryset(self):
        owner_queryset = self.queryset.filter(franchise__advertisement__user=self.request.user.id) \
            .exclude(franchise__advertisement__status__in=['ARCHIVED'])

        return owner_queryset

    def update(self, request, *args, **kwargs):
        instance = self.get_object()

        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        requirement_info = self.perform_update(serializer)

        read_serializer = FranchiseRequirementRetrieveSerializer(requirement_info)
        return Response(read_serializer.data, status=status.HTTP_200_OK)

    def perform_update(self, serializer):
        return serializer.save()


class FranchiseRequirementDocumentsDeleteViewSet(UpdateAPIView):
    lookup_field = 'pk'
    permission_classes = (IsAuthenticated, IsConfirmedUser,)
    queryset = FranchiseRequirement.objects.all()
    serializer_class = FranchiseRequirementDocumentsDeleteSerializer

    def get_queryset(self):
        owner_queryset = self.queryset.filter(franchise__advertisement__user=self.request.user.id) \
            .exclude(franchise__advertisement__status__in=['ARCHIVED'])

        return owner_queryset

    def update(self, request, *args, **kwargs):
        instance = self.get_object()

        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        requirement_info = self.perform_update(serializer)

        read_serializer = FranchiseRequirementRetrieveSerializer(requirement_info)
        return Response(read_serializer.data, status=status.HTTP_200_OK)

    def perform_update(self, serializer):
        return serializer.save()


class FranchiseAdminViewSet(GenericViewSet, mixins.ListModelMixin):
    queryset = Franchise.objects.all()
    permission_classes = (IsAuthenticated, IsAdmin,)
    serializer_class = FranchiseListSerializer
    pagination_class = SmallResultSetPagination


class FranchiseSearchViewSet(GenericViewSet, mixins.ListModelMixin, mixins.RetrieveModelMixin):
    queryset = Franchise.objects.filter(advertisement__status__in=['ACCEPTED'])
    permission_classes = (AllowAny,)
    serializer_class = FranchiseSearchRetrieveSerializer
    pagination_class = SmallResultSetPagination

    def get_serializer_class(self):
        if self.action in ['list']:
            return FranchiseListSerializer
        return FranchiseSearchRetrieveSerializer

    def get_queryset(self):
        queryset = self.queryset
        category = json.loads(self.request.query_params.get('category', '[]'))
        start_price = self.request.query_params.get('start_price')
        end_price = self.request.query_params.get('end_price')
        name = self.request.query_params.get('name')
        tariff_name = self.request.query_params.get('tariff_name')
        sort_type = self.request.query_params.get('sort_type')

        if category:
            queryset = queryset.filter(basic_info__category_id__in=category)
        if start_price:
            queryset = queryset.filter(additional_info__initial_fee__gt=start_price)
        if end_price:
            queryset = queryset.filter(additional_info__initial_fee__lt=end_price)
        if name:
            queryset = queryset.filter(basic_info__name__icontains=name)
        if tariff_name:
            queryset = queryset.filter(advertisement__tariff_name=tariff_name)

        if sort_type:
            if sort_type == 'date_desc':
                return queryset.order_by('-advertisement__created_at')
            elif sort_type == 'date_asc':
                return queryset.order_by('advertisement__created_at')
            elif sort_type == 'price_desc':
                return queryset.order_by('-additional_info__initial_fee')
            elif sort_type == 'price_asc':
                return queryset.order_by('additional_info__initial_fee')

        return queryset.order_by('-advertisement__created_at')

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        Advertisement.objects.filter(businesses=instance).update(view_count=F('view_count')+1)
        if instance.advertisement.view_count > 0 and instance.advertisement.view_count % 10 == 0:
            advertisement_views_notification(instance.advertisement.user)
        return Response(serializer.data)
