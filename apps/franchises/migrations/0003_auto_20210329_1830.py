# Generated by Django 3.1.6 on 2021-03-29 18:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('franchises', '0002_remove_franchiserequirement_representation_requirement'),
    ]

    operations = [
        migrations.AlterField(
            model_name='franchiseadditional',
            name='current_payments',
            field=models.CharField(blank=True, max_length=2048, null=True),
        ),
        migrations.AlterField(
            model_name='franchiseadditional',
            name='royalty_info',
            field=models.CharField(blank=True, max_length=2048, null=True),
        ),
        migrations.AlterField(
            model_name='franchiseadditional',
            name='short_description',
            field=models.CharField(max_length=2048),
        ),
        migrations.AlterField(
            model_name='franchisebasic',
            name='name',
            field=models.CharField(max_length=1024),
        ),
        migrations.AlterField(
            model_name='franchisecontact',
            name='contact',
            field=models.CharField(max_length=512),
        ),
        migrations.AlterField(
            model_name='franchisecontact',
            name='email',
            field=models.EmailField(max_length=512),
        ),
        migrations.AlterField(
            model_name='franchiserequirement',
            name='franchisee_requirement',
            field=models.CharField(max_length=2048),
        ),
        migrations.AlterField(
            model_name='franchiserequirement',
            name='package_content',
            field=models.CharField(max_length=2048),
        ),
    ]
