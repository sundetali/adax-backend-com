from typing import Dict, Any

from apps.notification.services import create_notification_base
from apps.users.models import User


@create_notification_base
def success_registration_notification(user: User) -> Dict[str, Any]:
    return {'name': 'Спасибо за регистрацию'}


@create_notification_base
def confirm_email_notification(user: User) -> Dict[str, Any]:
    return {'name': 'Ваша почта подтверждена'}
