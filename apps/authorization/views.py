import random

from django.contrib.sites.shortcuts import get_current_site
from django.db import transaction
from rest_framework import status
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from adax.utils import Mailer
from apps.authorization.serializers import UserLoginSerializer, UserSetPasswordSerializer
from apps.authorization.task import confirm_email_notification, success_registration_notification
from apps.billing import TariffType
from apps.billing.models import Tariff, TariffTypes
from apps.users.models import User
from apps.users.serializers import UserRegistrationSerializer, UserRetrieveSerializer


class UserRegistrationView(CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = UserRegistrationSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = self.perform_create(serializer)

        mailer = Mailer(mail_type='confirm_email', user=user)
        mailer.send_email(domain=get_current_site(request))
        success_registration_notification(user)
        read_serializer = UserRetrieveSerializer(user)

        headers = self.get_success_headers(read_serializer.data)
        return Response(read_serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    @transaction.atomic
    def perform_create(self, serializer):
        user = serializer.save()
        tariff_type, _ = TariffTypes.objects.get_or_create(name=TariffType.basic)
        Tariff.objects.create(user=user, type=tariff_type, payed=True)
        return user


class UserConfirmEmailView(APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        email = request.query_params['email']
        confirm_key = request.query_params['confirm_key']

        user = User.objects.filter(email=email, confirm_key=confirm_key).first()

        if user:
            user.email_confirmed = True
            user.save()
            confirm_email_notification(user)
            return Response({'status': 'success'}, status=status.HTTP_200_OK)

        return Response({'status': 'failed'}, status=status.HTTP_404_NOT_FOUND)


class UserLoginView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = UserLoginSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class UserForgotPasswordView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        email = request.data['email']

        user = User.objects.filter(email=email).first()

        if user:
            user.recover_code = random.randint(100000, 999999)
            user.save()

            mailer = Mailer(mail_type='forgot_password', user=user)
            mailer.send_email(domain=get_current_site(request))

            return Response({'status': 'success'}, status=status.HTTP_200_OK)

        return Response({'status': 'failed'}, status=status.HTTP_404_NOT_FOUND)


class UserSetPassword(APIView):
    permission_classes = ()
    serializer_class = UserSetPasswordSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        assert serializer.is_valid(raise_exception=True)

        return Response(
            data=serializer.data,
            status=status.HTTP_200_OK,
        )
