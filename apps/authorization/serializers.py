from django.contrib.auth import authenticate
from rest_framework import serializers

from apps.users.models import User


class UserLoginSerializer(serializers.Serializer):
    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass

    email = serializers.CharField(max_length=512)
    password = serializers.CharField(max_length=512, write_only=True)
    token = serializers.CharField(max_length=255, read_only=True)

    def validate(self, data):
        email = data.get('email', None)
        password = data.get('password', None)

        user = authenticate(username=email, password=password)

        if user is None:
            raise serializers.ValidationError(
                'A user with this email and password is not found.'
            )
        try:
            user = User.objects.get(email=user.email)
        except User.DoesNotExist:
            raise serializers.ValidationError(
                'User with given email and password does not exists'
            )

        if not user.is_active:
            raise serializers.ValidationError(
                'This user has been deactivated.'
            )

        return {
            'email': user.email,
            'token': user.token
        }


class UserSetPasswordSerializer(serializers.Serializer):
    email = serializers.CharField(max_length=512)
    recover_code = serializers.IntegerField(write_only=True)
    password = serializers.CharField(max_length=512, write_only=True)

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass

    def validate(self, data):
        email = data.get('email', None)
        recover_code = data.get('recover_code', None)
        password = data.get('password', None)

        user = User.objects.filter(email=email, recover_code=recover_code).first()

        if not user:
            raise serializers.ValidationError(
                'Wrong recover code or email.'
            )

        user.set_password(password)
        user.save()

        return {
            'email': user.email
        }
