from django.urls import path

from apps.authorization.views import UserRegistrationView, UserConfirmEmailView, UserLoginView, UserForgotPasswordView, \
    UserSetPassword

app_name = "authorization"

urlpatterns = [
    path('register/', UserRegistrationView.as_view(), name="user_register"),
    path('confirm-email/', UserConfirmEmailView.as_view(), name="user_confirm_email"),
    path('login/', UserLoginView.as_view(), name="user_login"),
    path('forgot-password/', UserForgotPasswordView.as_view(), name="user_forgot_password"),
    path('set-password/', UserSetPassword.as_view(), name="user_set_password"),
]
