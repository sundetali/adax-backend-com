from django.db.models import Manager


class OfferManager(Manager):
    def active(self):
        return self.filter(active=True)
