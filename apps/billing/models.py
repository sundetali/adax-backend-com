import datetime
from django.db import models

from apps.billing.managers import OfferManager
from apps.users.models import User
from apps.billing import OfferChoice, TariffType


def deadline_30():
    return datetime.date.today() + datetime.timedelta(days=30)


def default_date():
    return datetime.date(1666, 12, 12)


class TariffTypes(models.Model):
    name = models.CharField(max_length=150, choices=TariffType.TYPES, default=TariffType.basic, unique=True)
    description = models.TextField()
    price = models.IntegerField(default=0)
    number_ads = models.IntegerField(default=1)

    def __str__(self):
        return self.name


class Offer(models.Model):
    type = models.CharField(max_length=150, choices=OfferChoice.OFFERS, unique=True)
    name = models.TextField()
    description = models.TextField(null=True, blank=True)
    active = models.BooleanField(default=False)
    send = models.DateTimeField(auto_now=True, null=True)
    discount = models.PositiveIntegerField()
    users = models.ManyToManyField(User, related_name='offers', blank=True)
    objects = OfferManager()

    def save(self, *args, **kwargs):
        if self.active:
            try:
                temp = Offer.objects.get(active=True)
                if self != temp:
                    temp.active = False
                    temp.save()
            except Offer.DoesNotExist:
                pass
        super(Offer, self).save(*args, **kwargs)

    def __str__(self):
        return self.type


class Tariff(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='tariff')
    type = models.ForeignKey(TariffTypes, on_delete=models.CASCADE, related_name='tariffs')
    deadline = models.DateField(default=default_date())
    price = models.IntegerField(default=0)
    discount = models.IntegerField(default=0)
    payed = models.BooleanField()
    offer = models.ForeignKey(Offer, on_delete=models.CASCADE, null=True, blank=True, related_name='tariffs')

    @property
    def discount_price(self):
        if self.discount > 0:
            return round(self.price * (100 - self.discount) / 100)
        return 0

    @property
    def deadline_status(self):
        return self.deadline > datetime.date.today()

    def save(self, *args, **kwargs):
        self.price = self.type.price
        if self.payed:
            self.deadline = deadline_30()
        else:
            self.deadline = default_date()

        return super(Tariff, self).save(*args, **kwargs)


class TariffHistory(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='tariff_histories')
    type = models.ForeignKey(TariffTypes, on_delete=models.SET_NULL, null=True, related_name='tariff_histories')
    month = models.CharField(max_length=50)
