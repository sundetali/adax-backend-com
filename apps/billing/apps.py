from django.apps import AppConfig


class PaymentSystemConfig(AppConfig):
    name = 'apps.billing'

    def ready(self):
        import apps.billing.signals