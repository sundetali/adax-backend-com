from django.urls import path, include
from rest_framework import routers

from apps.billing.views import (
    TariffTypesListView,
    TariffViewSet,
    TariffOfferUpdateView,
    OfferView
)

app_name = 'billing'

router = routers.DefaultRouter()
router.register(r'', TariffViewSet)


urlpatterns = [
    path('tarifftypes/', TariffTypesListView.as_view()),
    path('tariff/', include(router.urls)),

    path('offer/', OfferView.as_view()),
    path('tariff/<int:pk>/offer/', TariffOfferUpdateView.as_view()),


]
