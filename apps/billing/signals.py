import calendar
from datetime import datetime, timedelta

from django.db.models.signals import post_save, m2m_changed
from django.db.models import Q
from django.dispatch import receiver

from apps.billing.models import Offer, Tariff, TariffHistory
from apps.billing.tasks import offer_notification
from apps.users.models import User
from apps.billing import TariffType, OfferChoice


@receiver(post_save, sender=Tariff)
def create_history(sender, instance, created, **kwargs):
    if not created and instance.payed:
        month = calendar.month_name[instance.deadline.month]
        TariffHistory.objects.create(user=instance.user, type=instance.type, month=month)


@receiver(post_save, sender=Offer)
def add_buss_prof_users(sender, instance, created, **kwargs):
    if not created and instance.active and instance.type != OfferChoice.personality:
        offer_type = instance.type
        if offer_type == OfferChoice.business_prof:
            users = User.objects.filter(tariff__type__name__in=[TariffType.business, TariffType.professional],
                                        tariff__payed=True)
            instance.users.clear()
            instance.users.add(*users)
        elif offer_type == OfferChoice.expired:
            now = datetime.now()
            users = User.objects.filter(
                Q(tariff__deadline=now+timedelta(days=7)) |
                Q(tariff__deadline=now+timedelta(days=3))
            )
            instance.users.clear()
            instance.users.add(*users)
        elif offer_type == OfferChoice.prof:
            users = User.objects.filter(tariff__type__name=TariffType.professional, tariff__payed=True)
            user_list = []

            for user in users:
                if {TariffType.professional} == set(user.tariff_histories.order_by('-id')[:2].values_list('type__name', flat=True)):
                    user_list.append(user)
            instance.users.clear()
            instance.users.add(*user_list)


@receiver(m2m_changed, sender=Offer.users.through)
def call_offer_notification(sender, instance, action, **kwargs):
    if action == 'post_add' or action == 'post_remove':
        for user in instance.users.all():
            offer_notification(user, instance)
