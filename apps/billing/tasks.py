import datetime
from typing import Dict, Any

from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
from apps.billing.models import Tariff, Offer
from apps.notification.services import create_notification_base
from apps.users.models import User


def limit_tariff_notification(user: User, tarif: Tariff) -> None:
    if tarif.deadline > datetime.date.today():
        days = str(tarif.deadline - datetime.date.today()).split()[0]

        if days == '7' or days == '3' or days == '1':
            async_to_sync(get_channel_layer().group_send)(
                str(user.id),
                {
                    'type': 'send_notification',
                    'message': f'Требуется оплатить за тариф через {days} дней'
                }
            )


@create_notification_base
def offer_notification(user: User, offer: Offer) -> Dict[str, Any]:
    text = {
        'type': offer.type,
        'name': offer.name,
        'description': offer.description
    }
    return text
