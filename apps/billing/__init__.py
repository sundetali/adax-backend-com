class TariffType:
    basic = 'BASIC'
    business = 'BUSINESS'
    professional = 'PROFESSIONAL'

    TYPES = (
        (basic, 'basic'),
        (business, 'business'),
        (professional, 'professional')
    )


class OfferChoice:
    personality = 'PERSONALITY'
    business_prof = 'BUSINESS_PROF'
    expired = 'EXPIRED'
    prof = 'PROFESSIONAL'

    OFFERS = (
        (personality, 'personality'),
        (business_prof, 'business_prof'),
        (expired, 'expired'),
        (prof, 'prof'),
    )

