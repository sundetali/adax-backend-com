from django.contrib import admin

from apps.billing.models import Tariff, TariffTypes, Offer
from apps.billing import OfferChoice


@admin.register(Tariff)
class TariffAdmin(admin.ModelAdmin):
    list_display = ['user', 'type']


@admin.register(Offer)
class OfferAdmin(admin.ModelAdmin):
    readonly_fields = ['send']

    def get_readonly_fields(self, request, obj=None):
        if not obj or obj.type != OfferChoice.personality:
            return self.readonly_fields + ['users']
        return self.readonly_fields


admin.site.register(TariffTypes)
