from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from . import TariffType
from .models import TariffTypes, Tariff, Offer


class TariffTypesShortSerializer(serializers.ModelSerializer):
    class Meta:
        model = TariffTypes
        fields = ['id', 'name', 'price']


class TariffTypesRetrieveSerializer(serializers.ModelSerializer):
    class Meta:
        model = TariffTypes
        fields = '__all__'


class OfferShortRetrieveSerializer(serializers.ModelSerializer):
    class Meta:
        model = Offer
        fields = ['id', 'type', 'name', 'discount']


class OfferRetrieveSerializer(serializers.ModelSerializer):
    class Meta:
        model = Offer
        fields = '__all__'


class TariffRetrieveSerializer(serializers.ModelSerializer):
    type = TariffTypesShortSerializer()
    deadline_status = serializers.ReadOnlyField()
    discount_price = serializers.ReadOnlyField()
    offer = OfferShortRetrieveSerializer()

    class Meta:
        model = Tariff
        fields = ['id', 'type', 'price', 'discount_price',
                  'deadline', 'deadline_status', 'offer', 'payed']


class TariffUpdateSerializer(serializers.Serializer):
    type = serializers.PrimaryKeyRelatedField(queryset=TariffTypes.objects.all())
    payed = serializers.BooleanField()

    def validate_type(self, value):
        if value.name == TariffType.basic and \
                self.context['request'].user.advertisements.filter(tariff_name=TariffType.basic).count():
            raise ValidationError('Basic tariff used')
        return value

    def update(self, instance, validated_data):
        instance.type = validated_data['type']
        instance.payed = validated_data['payed']
        instance.discount = 0
        instance.offer = None
        instance.save()
        return instance


class TariffOfferUpdateSerializer(serializers.Serializer):
    offer = serializers.PrimaryKeyRelatedField(queryset=Offer.objects.active())
    payed = serializers.BooleanField()

    def update(self, instance, validated_data):
        instance.offer = validated_data['offer']
        instance.discount = validated_data['offer'].discount
        instance.payed = validated_data['payed']
        instance.save()
        return instance

