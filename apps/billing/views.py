from rest_framework import mixins
from rest_framework.generics import ListAPIView, UpdateAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from adax.permissions import IsConfirmedUser
from apps.billing import TariffType
from apps.billing.models import TariffTypes, Tariff, Offer
from apps.billing.serializers import (
    TariffTypesRetrieveSerializer,
    TariffUpdateSerializer,
    TariffRetrieveSerializer,
    TariffOfferUpdateSerializer,
    OfferRetrieveSerializer
)
from apps.billing.tasks import limit_tariff_notification


class TariffTypesListView(ListAPIView):
    queryset = TariffTypes.objects.all()
    serializer_class = TariffTypesRetrieveSerializer
    permission_classes = [AllowAny]


class TariffViewSet(mixins.UpdateModelMixin,
                    mixins.DestroyModelMixin,
                    mixins.ListModelMixin,
                    GenericViewSet):
    queryset = Tariff.objects.all()
    permission_classes = [IsAuthenticated, IsConfirmedUser]

    def get_queryset(self):
        return self.queryset.filter(user=self.request.user)

    def get_serializer_class(self):
        if self.action == 'update':
            return TariffUpdateSerializer
        return TariffRetrieveSerializer

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        tariff = serializer.save()
        return Response(TariffRetrieveSerializer(tariff).data)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        limit_tariff_notification(request.user, queryset.first())
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.type = TariffTypes.objects.get(name=TariffType.basic)
        instance.payed = False
        instance.offer = None
        instance.save()
        return Response(TariffRetrieveSerializer(instance).data)


class OfferView(ListAPIView):
    queryset = Offer.objects.active()
    serializer_class = OfferRetrieveSerializer


class TariffOfferUpdateView(UpdateAPIView):
    queryset = Tariff.objects.all()
    permission_classes = [IsAuthenticated, IsConfirmedUser]
    serializer_class = TariffOfferUpdateSerializer

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        tariff = serializer.save()
        return Response(TariffRetrieveSerializer(tariff).data)

