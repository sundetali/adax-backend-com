from django.contrib import admin

from .models import FAQ, FAQStatistic

admin.site.register(FAQ)


@admin.register(FAQStatistic)
class FAQStatisticAdmin(admin.ModelAdmin):
    list_display = ['faq', 'answer']

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False
