from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from .models import FAQ, FAQStatistic
from . import FaqStatisticAnswer


class FAQBaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = FAQ
        fields = '__all__'


class FAQStatisticBaseSerializer(serializers.ModelSerializer):
    def validate(self, attrs):
        if attrs['answer'] == FaqStatisticAnswer.no and not attrs.get('text'):
            raise ValidationError('text key has empty value or does not exist')
        return attrs

    class Meta:
        model = FAQStatistic
        fields = '__all__'
