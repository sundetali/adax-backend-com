from requests import Response
from rest_framework.generics import ListAPIView, ListCreateAPIView
from rest_framework.permissions import AllowAny

from .models import FAQ, FAQStatistic
from .serialziers import FAQBaseSerializer, FAQStatisticBaseSerializer


class FAQListView(ListAPIView):
    queryset = FAQ.objects.all()
    serializer_class = FAQBaseSerializer
    permission_classes = [AllowAny]


class FAQStatisticListCreateView(ListCreateAPIView):
    queryset = FAQStatistic.objects.all()
    serializer_class = FAQStatisticBaseSerializer
    permission_classes = [AllowAny]
