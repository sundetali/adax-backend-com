class FaqStatisticAnswer:
    yes = 'YES'
    no = 'NO'

    ANSWER = (
        (yes, 'yes'),
        (no, 'no')
    )
