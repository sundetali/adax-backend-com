from django.db import models
from . import FaqStatisticAnswer


class FAQ(models.Model):
    question = models.CharField(max_length=250)
    answer = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    changed = models.DateTimeField(auto_now=True)
    status = models.BooleanField(default=True)

    class Meta:
        db_table = 'api_faq_faq'

    def __str__(self):
        return self.question


class FAQStatistic(models.Model):
    faq = models.ForeignKey(FAQ, on_delete=models.CASCADE)
    answer = models.CharField(max_length=3, choices=FaqStatisticAnswer.ANSWER)
    text = models.TextField(null=True, blank=True)
