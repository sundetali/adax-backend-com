from django.urls import path


from .views import FAQListView, FAQStatisticListCreateView

app_name = 'faq'

urlpatterns = [
    path('', FAQListView.as_view()),
    path('statistic/', FAQStatisticListCreateView.as_view()),
]