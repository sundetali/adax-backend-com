from django.contrib import admin

from apps.advertisements.models import Advertisement

admin.site.register(
    [
        Advertisement
    ]
)
