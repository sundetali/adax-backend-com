from django.urls import path, include
from rest_framework import routers

from apps.advertisements.views import AdvertisementArchiveViewSet, AdvertisementAcceptViewSet, \
    AdvertisementRejectViewSet

app_name = "advertisements"

router = routers.DefaultRouter()
# router.register(r'', AdvertisementViewSet, basename='advertisement')

urlpatterns = [
    path('<int:pk>/archive/', AdvertisementArchiveViewSet.as_view(), name='advertisements_archive'),
    path('<int:pk>/accept/', AdvertisementAcceptViewSet.as_view(), name='advertisements_accept'),
    path('<int:pk>/reject/', AdvertisementRejectViewSet.as_view(), name='advertisements_reject'),
    path('', include(router.urls))
]
