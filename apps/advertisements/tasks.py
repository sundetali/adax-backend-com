from typing import Dict, Any

from apps.notification.services import create_notification_base
from apps.users.models import User


@create_notification_base
def advertisement_accept_notification(user: User) -> Dict[str, Any]:
    return {'name': 'Объявление одобрено'}


@create_notification_base
def advertisement_reject_notification(user: User) -> Dict[str, Any]:
    return {'name': 'Объявление отказано'}


@create_notification_base
def advertisement_views_notification(user: User) -> Dict[str, Any]:
    return {'name': 'У вас новые просмотры'}
