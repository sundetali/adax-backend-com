from rest_framework.exceptions import ValidationError

from apps.users.models import User
from apps.billing import TariffType
from apps.billing.models import Tariff


def validate_tariff_active(tariff: Tariff) -> None:
    if not tariff.payed:
        if tariff.type.name == TariffType.basic:
            raise ValidationError('The limit on the tariff plan BASIC has expired')
        raise ValidationError('Need to pay for the tariff')


def validate_tariff_deadline(tariff: Tariff) -> None:
    if not tariff.deadline_status:
        tariff.payed = False
        tariff.save()
        raise ValidationError('Payment term expired')


def validate_tariff(user: User) -> None:
    tariff = user.tariff
    if tariff.type.name == TariffType.basic:
        validate_tariff_active(tariff)
        tariff.payed = False
        tariff.save()
    elif tariff.type.name == TariffType.business:
        validate_tariff_active(tariff)
        validate_tariff_deadline(tariff)
        tariff.payed = False
        tariff.save()
    elif tariff.type.name == TariffType.professional:
        validate_tariff_active(tariff)
        validate_tariff_deadline(tariff)

        ads_user = user.advertisements
        count_ads = ads_user.filter(tariff_name=TariffType.professional).count()

        if (count_ads+1) % tariff.type.number_ads == 0:
            tariff.payed = False
            tariff.save()

