from rest_framework import status
from rest_framework.generics import UpdateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from adax.permissions import IsAdmin
from apps.advertisements.models import Advertisement
from apps.advertisements.serializers import AdvertisementEmptySerializer
from apps.advertisements.tasks import advertisement_accept_notification, advertisement_reject_notification


class AdvertisementArchiveViewSet(UpdateAPIView):
    lookup_field = 'pk'
    permission_classes = (IsAuthenticated,)
    queryset = Advertisement.objects.all()
    serializer_class = AdvertisementEmptySerializer

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.status = 'ARCHIVED'
        instance.save()

        return Response(status=status.HTTP_200_OK)


class AdvertisementAcceptViewSet(UpdateAPIView):
    lookup_field = 'pk'
    permission_classes = (IsAuthenticated, IsAdmin,)
    queryset = Advertisement.objects.filter()
    serializer_class = AdvertisementEmptySerializer

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.status = 'ACCEPTED'
        instance.save()
        advertisement_accept_notification(request.user)
        return Response(status=status.HTTP_200_OK)


class AdvertisementRejectViewSet(UpdateAPIView):
    lookup_field = 'pk'
    permission_classes = (IsAuthenticated, IsAdmin,)
    queryset = Advertisement.objects.filter()
    serializer_class = AdvertisementEmptySerializer

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.status = 'REJECTED'
        instance.save()
        advertisement_reject_notification(request.user)
        return Response(status=status.HTTP_200_OK)

