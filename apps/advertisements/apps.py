from django.apps import AppConfig


class AdvertisementsConfig(AppConfig):
    name = 'apps.advertisements'
    verbose_name = 'Advertisements'
