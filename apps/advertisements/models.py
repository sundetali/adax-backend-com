from django.db import models
from safedelete.models import SafeDeleteModel

from apps.core.models import AdvertisementType, TimeStampedModel
from apps.users.models import User


class Advertisement(SafeDeleteModel, TimeStampedModel):
    STATUS = (
        ('CREATED', 'created'),
        ('UPDATED', 'updated'),
        ('ACCEPTED', 'accepted'),
        ('REJECTED', 'rejected'),
        ('ARCHIVED', 'archived')
    )

    type = models.ForeignKey(AdvertisementType, related_name='advertisements', on_delete=models.DO_NOTHING)
    user = models.ForeignKey(User, related_name='advertisements', on_delete=models.CASCADE)
    status = models.CharField(choices=STATUS, default='CREATED', max_length=32)
    tariff_name = models.CharField(max_length=150, null=True)
    view_count = models.PositiveIntegerField(default=0)

    def __str__(self):
        return str(self.id)

    class Meta:
        db_table = 'api_advertisements_advertisement'
        verbose_name = 'advertisement'
        verbose_name_plural = 'advertisements'
