import datetime
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from apps.advertisements.models import Advertisement
from apps.advertisements.tariff_validations import validate_tariff
from apps.core.serializers import AdvertisementTypeRetrieveSerializer


class AdvertisementSerializer(serializers.ModelSerializer):

    class Meta:
        model = Advertisement
        fields = ('type',)

    def validate(self, attrs):
        user = self.context['request'].user
        if not hasattr(user, 'tariff'):
            raise ValidationError('User does not have a tariff')
        return attrs

    def create(self, validated_data):
        # Advertisement
        user = self.context['request'].user
        validated_data['user'] = user
        validated_data['tariff_name'] = user.tariff.type.name
        validate_tariff(user)
        return Advertisement.objects.create(**validated_data)


class AdvertisementEmptySerializer(serializers.ModelSerializer):

    class Meta:
        model = Advertisement
        fields = ()


class AdvertisementRetrieveSerializer(serializers.ModelSerializer):
    type = AdvertisementTypeRetrieveSerializer()
    status = serializers.CharField(source='get_status_display')

    class Meta:
        model = Advertisement
        fields = ('pk', 'type', 'status', 'created_at', 'tariff_name', 'view_count')


class AdvertisementPkTitleSerializer(serializers.ModelSerializer):
    title = serializers.SerializerMethodField('get_advertisement_title')

    class Meta:
        model = Advertisement
        fields = ('id', 'title')

    def get_advertisement_title(self, obj):
        try:
            return obj.franchises.basic_info.name
        except Advertisement.franchises.RelatedObjectDoesNotExist:
            try:
                return obj.businesses.basic_info.name
            except Advertisement.businesses.RelatedObjectDoesNotExist:
                return "Empty"

