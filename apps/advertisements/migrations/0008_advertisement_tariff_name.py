# Generated by Django 3.1.6 on 2021-08-10 11:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('advertisements', '0007_auto_20210429_1455'),
    ]

    operations = [
        migrations.AddField(
            model_name='advertisement',
            name='tariff_name',
            field=models.CharField(max_length=150, null=True),
        ),
    ]
