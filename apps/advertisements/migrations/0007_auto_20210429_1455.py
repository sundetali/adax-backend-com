# Generated by Django 3.1.6 on 2021-04-29 14:55

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('advertisements', '0006_auto_20210329_1830'),
    ]

    operations = [
        migrations.AlterField(
            model_name='advertisement',
            name='created_at',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
