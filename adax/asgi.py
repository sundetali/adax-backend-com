"""
ASGI config for adax project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/asgi/
"""

import os

from channels.routing import ProtocolTypeRouter, URLRouter
from django.conf.urls import url
from django.core.asgi import get_asgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'adax.settings.production')
django_asgi_app = get_asgi_application()

from apps.authorization.backends import TokenAuthMiddlewareStack
from apps.chat.consumers import ChatNotifyConsumer
from apps.chat.consumers import ChatConsumer
from apps.notification.consumers import NotificationConsumer

application = ProtocolTypeRouter({
    "http": django_asgi_app,
    "websocket":
        TokenAuthMiddlewareStack(
            URLRouter(
                [
                    url("ws/chat-notify/", ChatNotifyConsumer.as_asgi()),
                    url(r"^ws/chat/(?P<user_id>\w+)/(?P<advertisement_id>\w+)/$", ChatConsumer.as_asgi()),
                    url(r"^ws/notification/(?P<user_id>[0-9]+)/$", NotificationConsumer.as_asgi())
                ]
            )
        )
})
