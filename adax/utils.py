from django.core.mail import EmailMessage
from django.template.loader import render_to_string


class Mailer(object):
    subjects = {
        'confirm_email': 'Активация аккаунта',
        'forgot_password': 'Восстановление пароля'
    }

    templates = {
        'confirm_email': 'adax/confirm_email.html',
        'forgot_password': 'adax/forgot_password.html'
    }

    def __init__(self, mail_type, user):
        self.subject = self.subjects[mail_type]
        self.template = self.templates[mail_type]
        self.user = user

    def send_email(self, domain):
        current_site = domain

        message = render_to_string(self.template, {
            'user': self.user,
            'domain': current_site.domain,
            'confirm_key': self.user.confirm_key
        })

        to_email = self.user.email
        email = EmailMessage(self.subject, message, to=[to_email])
        email.content_subtype = "html"
        email.send()
