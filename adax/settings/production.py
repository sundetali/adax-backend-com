from .base import *

DEBUG = False
ALLOWED_HOSTS = ['adax.kz', '46.101.148.44', 'localhost']

# MAIL
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'testersponge195@gmail.com'
EMAIL_HOST_PASSWORD = 'xbqcdnskdqdhhpnp'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER

# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'adax',
        'USER': 'adax_user',
        'PASSWORD': 'qX3guvatP4NMTJmd',
        'HOST': 'localhost',
        'PORT': '',
    }
}

CORS_ALLOW_CREDENTIALS = True
CORS_ALLOWED_ORIGINS = [
    "http://adax.newtone.kz",
    "http://localhost:8080",
    "http://46.101.148.44",
    "http://46.101.148.44:5000",
    "https://adax.newtone.kz",
    "http://evamarket.kz"
]
