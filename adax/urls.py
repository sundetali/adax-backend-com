""" ADAX URL Configuration """

from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions


schema_view = get_schema_view(
    openapi.Info(
        title="Adax API",
        default_version='v1',
        description="Adax API Description",
        terms_of_service="https://localhost/policies/terms/",
        contact=openapi.Contact(email="info@adax.com"),
        license=openapi.License(name="License"),
    ),

    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('admin/', admin.site.urls),

    path("api/advertisements/", include("apps.advertisements.urls", namespace="advertisements")),
    path("api/authorization/", include("apps.authorization.urls", namespace="authorization")),
    path("api/businesses/", include("apps.businesses.urls", namespace="businesses")),
    path("api/search/businesses/", include("apps.businesses.search_urls", namespace="businesses_search")),
    path("api/core/", include("apps.core.urls", namespace="core")),
    path("api/franchises/", include("apps.franchises.urls", namespace="franchises")),
    path("api/search/franchises/", include("apps.franchises.search_urls", namespace="franchise_search")),
    path("api/support/", include("apps.support.urls", namespace="support")),
    path("api/users/", include("apps.users.urls", namespace="users")),
    path("api/faq/", include("apps.faq.urls", namespace="faq")),
    path("api/billing/", include("apps.billing.urls", namespace="billing")),

    path("api/chat/", include("apps.chat.urls", namespace="chat")),
    path("api/notification/", include("apps.notification.urls", namespace="notification")),

    url(r'^docs/swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'^docs/redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
