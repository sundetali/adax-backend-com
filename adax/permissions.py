from rest_framework.permissions import BasePermission


class IsConfirmedUser(BasePermission):
    def has_permission(self, request, view):
        return request.user.email_confirmed


class IsAdmin(BasePermission):
    def has_permission(self, request, view):
        return request.user.is_admin
